import User from "../../Auth/models/user_model.js"
import Bus from "../models/bus_model.js";

const getTravelsName = async(bus_id) =>{
    const bus = await Bus.find({bus_id:bus_id},{travels_id:1});
    const travel_id = bus[0].travels_id;
    const detail = await User.find({_id:travel_id},{name:1});
    const name = detail[0].name;
    return name;
}

export default getTravelsName;