import Bus from "../models/bus_model.js";

// Travels add bus
const addBuses = async (bus_id,bus_name,bus_capacity,travels_id) => {
  const bus = new Bus({
    bus_id: bus_id,
    bus_name: bus_name,
    bus_capacity: bus_capacity,
    travels_id: travels_id,
  });
  return await bus.save();
};

export default addBuses;
