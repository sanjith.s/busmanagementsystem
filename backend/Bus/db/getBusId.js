import Bus from "../models/bus_model.js";

const getBusById = async (travel_id) => {
  const bus = await Bus.find({ travels_id: travel_id }, { bus_id: 1, _id: 0 });
  const bus_id = [];
  for (let i = 0; i < bus.length; i++) {
    bus_id.push(bus[i].bus_id);
  }

  return bus_id;
};

export default getBusById;
