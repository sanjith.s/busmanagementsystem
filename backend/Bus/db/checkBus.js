import CustomError from "../../utils/CustomError.js";
import Bus from "../models/bus_model.js";

const checkBus = async(bus_id)=>{
    const busCheck = await Bus.find({ bus_id: bus_id });
    if(busCheck.length === 0){
        return busCheck;
    }
    throw new CustomError("Bus id already exists", 400);
}

export default checkBus;