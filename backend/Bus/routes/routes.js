import express from "express";
const busRouter = express.Router();
busRouter.use(express.json());
busRouter.use(express.urlencoded({ extended: true }));

import verifyToken from "../../middleware/verifyToken.js";

// controllers
import addBus from "../../Bus/controllers/addBusTravels.js";
import getBusByTravelId from "../controllers/getBusByTravelId.js";
import getTravelName from "../controllers/getTravelName.js";
// const addBus = require("../../Bus/controllers/addBusTravels");
// const getBusByTravelId = require('../controllers/getBusByTravelId')

busRouter.post("/add-bus-travels", verifyToken, addBus);
busRouter.get('/get-bus/:travel_id',verifyToken,getBusByTravelId);
busRouter.get('/get-travel-name',getTravelName);

export default busRouter;