import mongoose from "mongoose";

const busSchema = new mongoose.Schema({
  bus_id: {
    type: String,
    required: [true , "Enter bus id"],
  },
  bus_name: {
    type: String,
    required: [true, "Enter bus name"],
  },
  bus_capacity: {
    type: String,
    required: [true,"Enter bus capacity"],
    min:[15,"Seat capacity should be atleast 15"],
    max:[120,"Seat capacity can be atmost 100"]
  },
  travels_id:{
    type:mongoose.Types.ObjectId,
    required:[true,"Enter travels id"],
    ref:'User'
  }
});

const Bus = mongoose.model("Bus", busSchema);

export default Bus;
