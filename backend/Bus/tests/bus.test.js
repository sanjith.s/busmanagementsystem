// For accessing the variables in .env files..
import { config } from "dotenv";
config();

import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";
import registerService from "../../Auth/services/registerService";
import addBusTravelSchema from "../services/addBusTravelService";
import getBusByTravelIdService from "../services/getBusByTravelIdService";
import { registerTravelMock } from "../../utils/mock";

let travel_id;
const role = "travels";

beforeAll(async () => {
  const mongoServer = await MongoMemoryServer.create();
  const mongoUri = mongoServer.getUri();

  await mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  const travel = await registerTravelMock();
  travel_id = travel._id.toString();
});

afterAll(async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
});

describe("Bus", () => {
  it("Add bus", async () => {
    // Add a bus
    const bus_id = "1";
    const bus_name = "Bus 1";
    const bus_capacity = 30;
    const bus = await addBusTravelSchema(
      travel_id,
      role,
      bus_id,
      bus_name,
      bus_capacity,
      travel_id
    );
    // check details
    expect(bus.bus_id).toBe(bus_id);
    expect(bus.bus_name).toBe(bus_name);
    expect(bus.travels_id.toString()).toBe(travel_id);
  });

  it("Add bus again", async () => {
    // Add a bus
    const bus_id = "1";
    const bus_name = "Bus 1";
    const bus_capacity = 30;
    const bus = await addBusTravelSchema(
      travel_id,
      role,
      bus_id,
      bus_name,
      bus_capacity,
      travel_id
    ).catch((err) => {
      expect(err.statusCode).toBe(400);
      expect(err.message).toBe("Bus id already exists");
    });
  });

  it("get bus by travel id", async () => {
    const getBus = await getBusByTravelIdService(travel_id, role, travel_id);
    // a travel must have a bus
    expect(getBus.length).toBeGreaterThan(0);
  });

  it("get bus by travel id with wrong role", async () => {
    // use role as "user"
    const getBus = await getBusByTravelIdService(
      travel_id,
      "user",
      travel_id
    ).catch((err) => {
      expect(err.statusCode).toBe(401);
      expect(err.message).toBe("Unauthorized");
    });
  });
});
