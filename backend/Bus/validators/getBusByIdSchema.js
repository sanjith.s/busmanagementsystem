import Joi from "joi";

const getBusByIdSchema = Joi.object().keys({
  travel_id: Joi.string().required().messages({
    "string.base": `Enter valid travel id`,
    "string.empty": `Travel id cannot be empty`,
    "any.required": `Travel id is required`,
  }),
});

export default getBusByIdSchema;
