import Joi from "joi";

const addBusSchema = Joi.object().keys({
  bus_id: Joi.string().required().messages({
    "string.base": `Enter valid bus id`,
    "string.empty": `Bus id cannot be empty`,
    "any.required": `Bus id is required`,
  }),
  bus_name: Joi.string().required().messages({
    "string.base": `Enter valid bus name`,
    "string.empty": `Bus name cannot be empty`,
    "any.required": `Bus name is required`,
  }),
  bus_capacity: Joi.number().min(10).required().messages({
    "number.min": "Bus capacity must be atleast 10",
    "number.base": `Enter valid bus capacity`,
    "number.empty": `Bus capacity cannot be empty`,
    "any.required": `Bus capacity is required`,
  }),
  travels_id: Joi.string().required().messages({
    "string.base": `Enter valid travels id`,
    "string.empty": `Travels id cannot be empty`,
    "any.required": `Travels id is required`,
  }),
});

export default addBusSchema;
