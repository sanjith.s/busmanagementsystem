// global try catch
import asyncErrorHandler from "../../utils/asyncErrorHandler.js";
import getBusByTravelIdService from "../services/getBusByTravelIdService.js";

// bus for the travel id
const getBusByTravelId = asyncErrorHandler(async (req, res) => {
  const { travel_id } = req.params;
  const bus_id = await getBusByTravelIdService(
    req.user.id,
    req.user.role,
    travel_id
  );
  res.status(200).json({ bus_id: bus_id });
});

export default getBusByTravelId;
