import asyncErrorHandler from "../../utils/asyncErrorHandler.js";
import getTravelNameService from "../services/getTravelNameService.js";

const getTravelName = asyncErrorHandler(async(req,res) =>{
    const {bus_id} = req.query;
    const travelName = await getTravelNameService(bus_id);
    res.status(200).send({name:travelName})
})

export default getTravelName;
