// global try catch
import asyncErrorHandler from "../../utils/asyncErrorHandler.js";

// services
import addBusTravelSchema from "../services/addBusTravelService.js";

// adding bus for trips (travels)
const addBus = asyncErrorHandler(async (req, res) => {
  const { bus_id, bus_name, bus_capacity, travels_id } = req.body;
  await addBusTravelSchema(
    req.user.id,
    req.user.role,
    bus_id,
    bus_name,
    bus_capacity,
    travels_id
  );
  res.status(200).send({ message: "Bus added successfully" });
});

export default addBus;
