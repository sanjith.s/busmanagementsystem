import addBusSchema from "../validators/addBusTravelsSchema.js";
import validate from "../../utils/validate.js";
import checkTravelRole from "../../utils/checkTravelRole.js";
import checkUserByID from "../../utils/checkUserByID.js";
import CustomError from "../../utils/CustomError.js";
import checkBus from "../db/checkBus.js";
import addBuses from "../db/addBus.js";

const addBusTravelSchema = async (
  id,
  role,
  bus_id,
  bus_name,
  bus_capacity,
  travels_id
) => {
  // validation using Joi
  if (!checkUserByID(id, travels_id)) {
    throw new CustomError("Enter valid travels id (your id)", 400);
  }
  if (
    checkTravelRole(role) &&
    validate(addBusSchema, { bus_id, bus_name, bus_capacity, travels_id })
  ) {
    await checkBus(bus_id);
    return await addBuses(bus_id, bus_name, bus_capacity, travels_id);
  }
};

export default addBusTravelSchema;
