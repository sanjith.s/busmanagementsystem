import checkBus from "../db/checkBus.js";
import getTravelsName from "../db/getTravelsName.js";

const getTravelNameService = async (bus_id)=>{
    return await getTravelsName(bus_id);
}

export default getTravelNameService;