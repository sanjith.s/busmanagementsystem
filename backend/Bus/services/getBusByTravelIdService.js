import checkTravelRole from "../../utils/checkTravelRole.js";
import validate from "../../utils/validate.js";
import getBusByIdSchema from "../validators/getBusByIdSchema.js";
import checkUserByID from "../../utils/checkUserByID.js";
import getBusById from "../db/getBusId.js";

const getBusByTravelIdService = async (id, role, travel_id) => {
  if (!checkUserByID(id, travel_id)) {
    throw new CustomError("Enter valid travels id (your id)", 400);
  }
  if (checkTravelRole(role) && validate(getBusByIdSchema, { travel_id })) {
    const bus_id = await getBusById(travel_id);
    return bus_id;
  }
};

export default getBusByTravelIdService;
