// For accessing the variables in .env files..
import { config } from "dotenv";
config();

import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";
import registerService from "../../Auth/services/registerService";
import addBusTravelSchema from "../../Bus/services/addBusTravelService";
import addTripService from "../services/addTripService";
import deleteTripService from "../services/deletetripService";
import updateTripService from "../services/updateTripService";
import viewTripUserService from "../services/viewTripUserService";
import viewTripTravelService from "../services/viewTripTravelService";
import { addBusMock, registerTravelMock } from "../../utils/mock";

let travel_id;
let bus_id;
let trip_id;

beforeAll(async () => {
  const mongoServer = await MongoMemoryServer.create();
  const mongoUri = mongoServer.getUri();

  await mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  const travel = await registerTravelMock();
  travel_id = travel._id.toString();
  bus_id = await addBusMock(travel_id);
});

afterAll(async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
});

describe("Trips", () => {
  const bus_capacity = 30;
  const today = new Date();
  const tomorrow = today.setDate(today.getDate() + 1); //setting tomorrow date
  it("add Trip", async () => {
    // Add trip
    const source = "Coimbatore";
    const destination = "Chennai";
    const price = 980;
    const trip_date = "8:25";
    const duration = 8;
    const capacity = bus_capacity;
    const available_days = [0, 1, 2, 3, 4, 5, 6];
    const trip = await addTripService(
      "travels",
      bus_id,
      source,
      destination,
      price,
      trip_date,
      duration,
      capacity,
      available_days
    );

    trip_id = trip._id.toString();
    expect(trip).toBeTruthy();
    expect(trip.bus_id).toBe(bus_id);
    expect(trip.source).toBe(source);
    expect(trip.destination).toBe(destination);
  });

  it("update Trip", async () => {
    // Update trip
    const bus_id = "1";
    const source = "Coimbatore";
    const destination = "Chennai";
    const price = 980;
    const trip_date = "8:25";
    const duration = 8;
    const capacity = 40;
    const available_days = [0, 1, 2, 3, 4, 5, 6];
    const trip = await updateTripService(
      "travels",
      trip_id,
      bus_id,
      source,
      destination,
      price,
      trip_date,
      duration,
      capacity,
      available_days
    );

    expect(trip).toBeTruthy();
  });

  it("view trips user", async () => {
    const source = "Coimbatore";
    const destination = "Chennai";
    const datetime = new Date(tomorrow);
    const viewTrips = await viewTripUserService(
      "user",
      source,
      destination,
      datetime
    );
    expect(viewTrips).toBeTruthy();
  });

  it("View trips travels", async () => {
    const role = "travels";
    const date = new Date(tomorrow);
    const viewTrips = await viewTripTravelService(role, trip_id, date);
    expect(viewTrips).toBeTruthy();
    expect(viewTrips[0]._id.toString()).toBe(trip_id);
  });

  it("delete trip", async () => {
    // delete trip with proper trip id
    const deleteTrip = await deleteTripService("travels", trip_id);
    expect(deleteTrip).toBeTruthy();
  });

  it("delete trip again", async () => {
    // delete trip with already deleted trip id
    const deleteTrip = await deleteTripService("travels", trip_id).catch(
      (err) => {
        expect(err.statusCode).toBe(400);
        expect(err.message).toBe("Trip not available");
      }
    );
  });
});
