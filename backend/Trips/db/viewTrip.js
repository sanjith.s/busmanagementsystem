import Trip from "../models/trips_model.js";
import Booking from "../../Booking/models/booking_model.js";

const viewTrips = async (source, destination, datetime) => {
  const trip = await Trip.find(
    {
      source: source,
      destination: destination,
      available_days: { $in: [new Date(datetime).getDay()] },
    },
    { available_days: 0 }
  );

  for (let i = 0; i < trip.length; i++) {
    const booking = await Booking.find({
      trip_id: trip[i]._id,
      trip_date: new Date(datetime),
      isCancelled: false,
    });

    let count = 0;
    for (let j = 0; j < booking.length; j++) {
      count += booking[j].count;
    }

    trip[i].filled_seats = count;
  }

  return trip;
};

export default viewTrips;
