import CustomError from "../../utils/CustomError.js";
import Trip from "../models/trips_model.js";

const checkTrip = async (trip_id) => {
  const tripCheck = await Trip.find({ _id: trip_id });
  if (tripCheck.length === 0) {
    throw new CustomError("Trip not available", 400);
  }
  return true;
};

export default checkTrip;
