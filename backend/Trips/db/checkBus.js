import CustomError from "../../utils/CustomError.js";
import Bus from "../../Bus/models/bus_model.js";

const checkBus = async(bus_id)=>{
    const busCheck = await Bus.find({ bus_id: bus_id });
    if(busCheck.length === 0){
        throw new CustomError("Bus not found", 400);
    }
    return busCheck;
}

export default checkBus;