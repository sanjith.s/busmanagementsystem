import CustomError from "../../utils/CustomError.js";
import Trip from "../models/trips_model.js";

const checkTripDate = async (trip_id,date) => {
  const tripCheck = await Trip.find({
    _id: trip_id,
    available_days: { $in: [new Date(date).getDay()] },
  });
  if (tripCheck.length === 0) {
    throw new CustomError("No trips available",200);
  }
  return tripCheck;
};

export default checkTripDate;
