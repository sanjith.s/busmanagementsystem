import Trip from "../models/trips_model.js";

const updateTrips = async (trip_id,
  bus_id,
  source,
  destination,
  price,
  dateTime,
  duration,
  capacity,
  available_days
) => {
  let updatedTrip = {
    bus_id: bus_id,
    source: source,
    destination: destination,
    price: price,
    dateTime: dateTime,
    duration: duration,
    capacity: capacity,
    available_days: available_days,
  };
  await Trip.updateOne(
    { _id: trip_id },
    {
      $set: updatedTrip,
    },
    { runValidators: true }
  );
  return true;
};

export default updateTrips;
