import Trip from "../models/trips_model.js";
import capitalizeWord from "../../utils/capitalizeWord.js";

const addTrips = async (
  bus_id,
  source,
  destination,
  price,
  dateTime,
  duration,
  capacity,
  available_days
) => {
  const trip = new Trip({
    bus_id: bus_id,
    source: capitalizeWord(source),
    destination: capitalizeWord(destination),
    price: price,
    dateTime: dateTime,
    duration: duration,
    capacity: capacity,
    available_days: available_days,
  });
  return await trip.save();
};

export default addTrips;
