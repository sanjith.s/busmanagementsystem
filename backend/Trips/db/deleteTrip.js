import Trip from "../models/trips_model.js";

const deleteTrips = async (trip_id) => {
    return await Trip.deleteOne({ _id: trip_id });
};

export default deleteTrips;