import Joi from "joi";

const deleteTripSchema = Joi.object().keys({
  trip_id: Joi.string().required().messages({
    "string.base": `Enter valid trip id`,
    "string.empty": `Trip id cannot be empty`,
    "any.required": `Trip id is required`,
  }),
});

export default deleteTripSchema;
