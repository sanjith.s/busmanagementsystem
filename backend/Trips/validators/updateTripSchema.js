import Joi from "joi";

const updateTripSchema = Joi.object().keys({
  trip_id: Joi.string().required().messages({
    "string.base": `Enter valid trip id`,
    "string.empty": `Trip id cannot be empty`,
    "any.required": `Trip id is required`,
  }),
  bus_id: Joi.string().required().messages({
    "string.base": `Enter valid bus id`,
    "string.empty": `Bus id cannot be empty`,
    "any.required": `Bus id is required`,
  }),
  source: Joi.string().required().messages({
    "string.base": `Enter valid source`,
    "string.empty": `Source cannot be empty`,
    "any.required": `Source is required`,
  }),
  destination: Joi.string().required().messages({
    "string.base": `Enter valid destination`,
    "string.empty": `Destination cannot be empty`,
    "any.required": `Destination is required`,
  }),
  price: Joi.number().min(10).required().messages({
    "number.base": `Enter valid price`,
    "number.min": `Price must be atleast 10`,
    "number.empty": `Price cannot be empty`,
    "any.required": `Price is required`,
  }),
  dateTime: Joi.string()
    // .regex(/^([0-9]{2})\:([0-9]{2})$/)
    .required()
    .messages({
      "string.base": `Enter valid time`,
      "string.empty": `Time cannot be empty`,
      "string.required": `Time is required`,
    }),
  duration: Joi.number().min(1).required().messages({
    "number.base": `Enter valid duration`,
    "number.empty": `Duration cannot be empty`,
    "number.min": `Duration can be atleast 1`,
    "any.required": `Duration is required`,
  }),
  capacity: Joi.number().min(10).required().messages({
    "number.base": `Enter valid capacity`,
    "number.empty": `Capacity cannot be empty`,
    "any.required": `Capacity is required`,
  }),
  available_days: Joi.array()
    .min(1)
    .max(7)
    .items(Joi.number().min(0).max(6).required())
    .required()
    .messages({
      "array.base": `Enter valid available days`,
      "array.empty": `Available days cannot be empty`,
      "any.required": `Available days is required`,
      "array.min": "Trip should be atleast a day",
      "array.max": "Trip can run only seven times in a week",
      "number.min": "Enter valid day",
      "number.max": "Enter valid day",
    }),
});

export default updateTripSchema;
