import Joi from "joi";

const viewTripUserSchema = Joi.object().keys({
  source: Joi.string().required().messages({
    "string.base": `Enter valid source`,
    "string.empty": `Source cannot be empty`,
    "any.required": `Source is required`,
  }),
  destination: Joi.string().required().messages({
    "string.base": `Enter valid destination`,
    "string.empty": `Destination cannot be empty`,
    "any.required": `Destination is required`,
  }),
  datetime: Joi.date().required().messages({
    "date.base": "Enter a valid date",
    "date.empty": `Date cannot be empty`,
    "any.required": `Date is required`,
  }),
});

export default viewTripUserSchema;
