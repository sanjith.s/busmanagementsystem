import Joi from "joi";

const viewTripTravelSchema = Joi.object().keys({
  trip_id: Joi.string().required().messages({
    "string.base": `Enter valid trip id`,
    "string.empty": `Trip id cannot be empty`,
    "any.required": `Trip id is required`,
  }),
  date: Joi.date().required().messages({
    "date.base": "Enter a valid date",
    "date.empty": `Date cannot be empty`,
    "any.required": `Date is required`,
  }),
});

export default viewTripTravelSchema;
