import CustomError from "../../utils/CustomError.js";
import checkDate from "../../utils/checkDate.js";
import checkUserRole from "../../utils/checkUserRole.js";
import validate from "../../utils/validate.js";
import viewTrips from "../db/viewTrip.js";
import viewTripUserSchema from "../validators/viewTripUserSchema.js";

const viewTripUserService = async (role, source, destination, datetime) => {
  if (
    checkUserRole(role) &&
    validate(viewTripUserSchema, { source, destination, datetime })
  ) {
    if (!checkDate(datetime)) {
      throw new CustomError("Enter correct date", 400);
    }
    const trip = await viewTrips(source, destination, datetime);
    if (trip.length != 0) {
      return trip;
    } else {
      // since no bus on source and destination
      throw new CustomError("Bus not found", 400);
    }
  }
};

export default viewTripUserService;
