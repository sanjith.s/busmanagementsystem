import CustomError from "../../utils/CustomError.js";
import checkTravelRole from "../../utils/checkTravelRole.js";
import validate from "../../utils/validate.js";
import checkTrip from "../db/checkTrip.js";
import updateTrips from "../db/updateTrip.js";
import updateTripSchema from "../validators/updateTripSchema.js";

const updateTripService = async (
  role,
  trip_id,
  bus_id,
  source,
  destination,
  price,
  dateTime,
  duration,
  capacity,
  available_days
) => {
  if (
    checkTravelRole(role) &&
    validate(updateTripSchema, {
      trip_id,
      bus_id,
      source,
      destination,
      price,
      dateTime,
      duration,
      capacity,
      available_days,
    })
  ) {
    const tripCheck = await checkTrip(trip_id);
    if (!tripCheck) {
      throw new CustomError("Trip unavailable", 400);
    }
    if (
      await updateTrips(
        trip_id,
        bus_id,
        source,
        destination,
        price,
        dateTime,
        duration,
        capacity,
        available_days
      )
    ) {
      return true;
    } else {
      throw new CustomError("Something went wrong", 500);
    }
  }
};

export default updateTripService;
