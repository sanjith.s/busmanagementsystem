import checkBus from "../db/checkBus.js"
import CustomError from "../../utils/CustomError.js";
import checkTravelRole from "../../utils/checkTravelRole.js";
import validate from "../../utils/validate.js";
import addTrips from "../db/addTrip.js";
import addTripSchema from "../validators/addTripSchema.js";

const addTripService = async (
  role,
  bus_id,
  source,
  destination,
  price,
  dateTime,
  duration,
  capacity,
  available_days
) => {
  if (
    checkTravelRole(role) &&
    validate(addTripSchema, {
      bus_id,
      source,
      destination,
      price,
      dateTime,
      duration,
      capacity,
      available_days,
    })
  ) {
    // Initially filled seats is 0
    await checkBus(bus_id);

    return await addTrips(
      bus_id,
      source,
      destination,
      price,
      dateTime,
      duration,
      capacity,
      available_days
    );
  }
};

export default addTripService;
