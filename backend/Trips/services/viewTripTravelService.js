import checkTravelRole from "../../utils/checkTravelRole.js";
import validate from "../../utils/validate.js";
import viewTripTravelSchema from "../validators/viewTripTravelSchema.js";
import checkTripDate from "../db/checkTripDate.js"
import CustomError from "../../utils/CustomError.js";

const viewTripTravelService = async (role, trip_id, date) => {
  if (
    checkTravelRole(role) &&
    validate(viewTripTravelSchema, { trip_id, date })
  ) {
    const tripCheck = await checkTripDate(trip_id, date);
    return tripCheck;
  }
};

export default viewTripTravelService;
