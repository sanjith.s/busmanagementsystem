import CustomError from "../../utils/CustomError.js";
import checkTravelRole from "../../utils/checkTravelRole.js";
import validate from "../../utils/validate.js";
import checkTrip from "../db/checkTrip.js";
import deleteTrips from "../db/deleteTrip.js";
import deleteTripSchema from "../validators/deleteTripSchema.js"

const deleteTripService = async (role, trip_id) => {
  if (checkTravelRole(role) && validate(deleteTripSchema, { trip_id })) {
     await checkTrip(trip_id);
    
    if (await deleteTrips(trip_id)) {
      return true;
    } else {
      throw new CustomError("Something went wrong!", 500);
    }
  }
};

export default deleteTripService;
