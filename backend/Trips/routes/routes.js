import express from "express";
const tripRouter = express.Router();
tripRouter.use(express.json());
tripRouter.use(express.urlencoded({ extended: true }));

import verifyToken from "../../middleware/verifyToken.js";

// travels
import addTrip from "../../Trips/controllers/addTrips.js";
import deleteTrip from "../../Trips/controllers/deleteTrip.js";
import updateTrip from "../../Trips/controllers/updateTrip.js";
import viewTripsTravels from "../../Trips/controllers/viewTripsTravels.js";
// const addTrip = require("../../Trips/controllers/addTrips");
// const deleteTrip = require("../../Trips/controllers/deleteTrip");
// const updateTrip = require("../../Trips/controllers/updateTrip");
// const viewTripsTravels = require("../../Trips/controllers/viewTripsTravels");

// user
import viewTrip from "../controllers/viewTrips.js";
// const viewTrip = require("../controllers/viewTrips");

tripRouter.post("/add-trip", verifyToken, addTrip);

tripRouter.delete("/delete-trip", verifyToken, deleteTrip);

tripRouter.patch("/update-trip", verifyToken, updateTrip);

tripRouter.get("/view-trips-travel/:trip_id/", verifyToken, viewTripsTravels);

// viewTrips for user..
tripRouter.get("/view-trip", verifyToken, viewTrip);

export default tripRouter;
