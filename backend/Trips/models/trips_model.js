import mongoose from "mongoose";

const tripSchema = new mongoose.Schema({
  // trip_id: {
  //     // can change to _id later
  //     type: mongoose.Schema.Types.ObjectId
  //   },
  bus_id: {
    // may be foreign key constraint
    type: String,
    required: [true, "Enter bus id"],
    ref:'Bus'
  },
  source: {
    type: String,
    required: [true, "Enter source"],
  },
  destination: {
    type: String,
    required: [true,"Enter destination"],
  },
  price: {
    type: Number,
    required: [true,'Enter price'],
    min:[10,"Minimum price should be greater than 10"]
  },
  dateTime: {
    // type: Date,
    type:String,
    required: [true, "Enter time"],
  },
  duration:{
    type:String,
    required:[true,"Enter duration"]
  },
  capacity: {
    type: Number,
    required: [true,"Enter capacity"],
    min:10
  },
  filled_seats: {
    type: Number,
    required: [true,"Enter filled seats"],
    default: 0, //since no tickets booked at initial
  },
  available_days: {
    type: Array,
    required: [true,"Enter atleast one day"],
  },
});

const Trip = mongoose.model("Trip", tripSchema);

export default Trip;
