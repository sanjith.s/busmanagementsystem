// global try catch
import asyncErrorHandler from "../../utils/asyncErrorHandler.js";

// services
import viewTripUserService from "../services/viewTripUserService.js";

// view trips for user by entering source , destination and date
const viewTrip = asyncErrorHandler(async (req, res) => {
  const { source, destination, datetime } = req.query;

  const trip = await viewTripUserService(
    req.user.role,
    source,
    destination,
    datetime
  );

  res.status(200).json({ trip: trip });
});

export default viewTrip;
