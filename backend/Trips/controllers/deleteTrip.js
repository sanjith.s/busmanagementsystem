// global try catch
import asyncErrorHandler from "../../utils/asyncErrorHandler.js";

// service function
import deleteTripService from "../services/deletetripService.js";

const deleteTrip = asyncErrorHandler(async (req, res) => {
  const { trip_id } = req.body;
  await deleteTripService(req.user.role, trip_id);
  res.status(200).send({ message: "Trip deleted successfully" });
});

export default deleteTrip;
