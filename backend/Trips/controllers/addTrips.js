// global try catch
import asyncErrorHandler from "../../utils/asyncErrorHandler.js";

// service functions
import addTripService from "../services/addTripService.js";

// Add trips
const addTrip = asyncErrorHandler(async (req, res) => {
  const {
    bus_id,
    source,
    destination,
    price,
    dateTime,
    duration,
    capacity,
    available_days,
  } = req.body;

  await addTripService(
    req.user.role,
    bus_id,
    source,
    destination,
    price,
    dateTime,
    duration,
    capacity,
    available_days
  );
  res
    .status(200)
    .send({ message: "Trip added successfully for the bus " + bus_id });
});

export default addTrip;
