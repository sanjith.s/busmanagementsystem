// global try catch
import asyncErrorHandler from "../../utils/asyncErrorHandler.js";

// service functions
import updateTripService from "../services/updateTripService.js";

const updateTrip = asyncErrorHandler(async (req, res) => {
  const {
    trip_id,
    bus_id,
    source,
    destination,
    price,
    dateTime,
    duration,
    capacity,
    available_days,
  } = req.body;
  await updateTripService(
    req.user.role,
    trip_id,
    bus_id,
    source,
    destination,
    price,
    dateTime,
    duration,
    capacity,
    available_days
  );
  res.status(200).send({ message: "Trip updated successfully" });
});

export default updateTrip;
