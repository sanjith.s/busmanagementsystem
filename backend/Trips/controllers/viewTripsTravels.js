// global try catch
import asyncErrorHandler from "../../utils/asyncErrorHandler.js";

// service function
import viewTripTravelService from "../services/viewTripTravelService.js";

// View trips for travels..

const viewTripsTravels = asyncErrorHandler(async (req, res) => {
  const { trip_id } = req.params;
  const { date } = req.query;

  const trips = await viewTripTravelService(req.user.role, trip_id, date);
  res.status(200).send({ trips: trips });
});

export default viewTripsTravels;
