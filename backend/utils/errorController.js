// For accessing the variables in .env files..
import { config } from "dotenv";
config();

import CustomError from "../utils/CustomError.js";

const devErrors = (res, error) => {
  return res.status(error.statusCode).json({
    status: error.statusCode,
    message: error.message,
    stackTrace: error.stackTrace,
    error: error,
  });
};

const prodErrors = (res, error) => {
  if (error.isOperational) {
    return res.status(error.statusCode).json({
      status: error.statusCode,
      message: error.message,
    });
  } else {
    return res.status(500).json({
      status: "error",
      message: "Something went wrong! Pls try again later",
    });
  }
};

// when a wrong object id is given , it executes
const castErroHandler = (error) => {
  const msg = `Invalid value for ${error.path} : ${error.value}`;
  return new CustomError(msg, 400);
};

// Try to insert same name as in DB(unique)
const duplicateKeyErrorHandler = (error) =>{
  const msg = `There is already a email with ${error.keyValue.email}. Please use another email`
  return new CustomError(msg,400)
}

// when it exceeds the value (min or max) in schema
const validationErrorHandler = (error) =>{
  const err = Object.values(error.errors).map((val)=>val.message);
  const errorMessage = err.join('. ')
  const msg = `Invalid input data : ${errorMessage}`
  
  return new CustomError(msg,400);
}

const globalErrorHandler = (error, req, res, next) => {
  error.statusCode = error.statusCode || 500;
  error.status = error.status || "error";
  if (process.env.NODE_ENV === "development") {
    devErrors(res, error);
  } else if (process.env.NODE_ENV === "production") {
    // let err = {error , name:error.name}
    if (error.name === "CastError") {
      error = castErroHandler(error);
    }
    if(error.code === 11000){
      error = duplicateKeyErrorHandler(error)
    }
    if(error.name === 'ValidationError'){
      error = validationErrorHandler(error);
    }
    prodErrors(res, error);
  }
};

export default globalErrorHandler;