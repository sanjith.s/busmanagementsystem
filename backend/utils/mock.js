import registerService from "../Auth/services/registerService.js";
import addBusTravelSchema from "../Bus/services/addBusTravelService.js";
import addTripService from "../Trips/services/addTripService.js";

export const registerUserMock = async () => {
  const name = "user1";
  const email = "user1@gmail.com";
  const password = "user123";
  const role = "user";
  const phone = "9887766554";
  const user = await registerService(name, email, password, role, phone);
  return user;
};

export const registerTravelMock = async () => {
  const travel_name = "travels1";
  const travel_email = "travels1@gmail.com";
  const travel_password = "travels123";
  const travel_role = "travels";
  const travel_phone = "9887766554";
  const travel = await registerService(
    travel_name,
    travel_email,
    travel_password,
    travel_role,
    travel_phone
  );
  return travel;
};

export const addBusMock = async (travel_id) => {
  const bus_id = "1";
  const bus_name = "Bus 1";
  const bus_capacity = 30;
  const bus = await addBusTravelSchema(
    travel_id,
    "travels",
    bus_id,
    bus_name,
    bus_capacity,
    travel_id
  );
  return bus_id;
};

export const addTripMock = async (bus_id) => {
  const source = "Coimbatore";
  const destination = "Chennai";
  const price = 980;
  const trip_date = "8:25";
  const duration = 8;
  const capacity = 30;
  const available_days = [0, 1, 2, 3, 4, 5, 6];
  const trip = await addTripService(
    "travels",
    bus_id,
    source,
    destination,
    price,
    trip_date,
    duration,
    capacity,
    available_days
  );
  return trip;
};
