import CustomError from "./CustomError.js";

const checkUserRole = (role) =>{
    if(role === "user" ){
        return true;
    }
    throw new CustomError("Unauthorized",401);
}

export default checkUserRole;