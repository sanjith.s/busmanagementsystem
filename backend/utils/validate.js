// utils
import CustomError from "./CustomError.js";

// JOI validation
const validate = (schema, body) => {
  const result = schema.validate(body);
  if (result?.error) {
    throw new CustomError(result.error.details[0].message, 400);
  }
  return true;
};

export default validate;
