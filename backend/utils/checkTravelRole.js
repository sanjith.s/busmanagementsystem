import CustomError from "./CustomError.js";

const checkTravelRole = (role) =>{
    if(role === "travels" ){
        return true;
    }
    throw new CustomError("Unauthorized",401);
}

export default checkTravelRole;