const checkDate = (dateTime) => {
  if (
    new Date(dateTime).setHours(0, 0, 0, 0) < new Date().setHours(0, 0, 0, 0)
  ) {
    return false;
  }
  return true;
};

export default checkDate;
