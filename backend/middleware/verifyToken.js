import jwt from "jsonwebtoken";
const { sign, verify } = jwt;

function verifyToken(req, res, next) {
  const authHeader = req.headers["authorization"];

  const bearer = authHeader && authHeader.split(" ")[0];
  const token = authHeader && authHeader.split(" ")[1];

  if (token == null || bearer !== "Bearer") return res.sendStatus(401);
  try {
    const decoded = jwt.verify(token, process.env.TOKEN_SECRET.toString());
    req.user = decoded;
    next();
  } catch (error) {
    console.log(error);
    return res.sendStatus(403); 
  }
}

export default verifyToken;
