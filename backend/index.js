// For accessing the variables in .env files..
import { config } from "dotenv";
config();

import express, { json, urlencoded } from "express";
import { connect, mongoose } from "mongoose";

const app = express();

import router from "./source/routes/routes.js";

import CustomError from "./utils/CustomError.js";
import globalErrorHandler from "./utils/errorController.js";

app.use(json());
app.use(urlencoded({ extended: true }));

import cors from "cors";
const corsOptions = {
  origin: "*",
  credentials: true, //access-control-allow-credentials:true
  optionSuccessStatus: 200,
};

app.use(cors(corsOptions));

app.use("/", router);

// unhandled routed
app.all("*", (req, res, next) => {
  // const error = new Error("Url not found");
  // error.statusCode = 404;
  // error.status = "fail";
  const error = new CustomError("Url not found", 404);
  next(error);
});

// Global error handling
app.use(globalErrorHandler);

// Mongoose connection using then
// connect(process.env.MONGODB_URL)
//   .then(() => {
//     console.log("Connected to the database ");
//     app.listen(process.env.PORT || 8080, () => {
//       console.log("Server is running");
//     });
//   })
//   .catch((err) => {
//     console.error(`Error connecting to the database. n${err}`);
//   });

connect(process.env.MONGODB_URL);
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => {
  console.log("Connected to database");
  app.listen(process.env.PORT || 8080, () => {
    console.log("Server is running");
  });
});

export default app;
