import express from "express";
const bookingRouter = express.Router();
bookingRouter.use(express.json());
bookingRouter.use(express.urlencoded({ extended: true }));

import verifyToken from "../../middleware/verifyToken.js";

// travels
import viewBookingTravel from "../../Booking/controllers/viewBookingTravel.js";

// User
import bookTrip from "../../Booking/controllers/bookTrip.js";
import viewBoookingUser from "../../Booking/controllers/viewBookingUser.js";
import cancelTicket from "../../Booking/controllers/cancelTicket.js";
import getSeats from "../controllers/getSeats.js";

// travels api
bookingRouter.get('/view-booking-travels/:trip_id/',verifyToken,viewBookingTravel);

// users api
bookingRouter.post('/book-trip',verifyToken,bookTrip);
bookingRouter.post('/cancel-ticket',verifyToken,cancelTicket);
bookingRouter.get('/view-bookings-user/:user_id',verifyToken,viewBoookingUser);
bookingRouter.get('/get-seats',verifyToken,getSeats);

export default bookingRouter;