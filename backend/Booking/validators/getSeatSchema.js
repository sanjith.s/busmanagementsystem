import Joi from "joi";

const getSeatSchema = Joi.object().keys({
  trip_id: Joi.string().required().messages({
    "string.base": `Enter valid trip id`,
    "string.empty": `Trip id cannot be empty`,
    "any.required": `Trip id is required`,
  }),
  date: Joi.date().required().messages({
    "date.base": `Enter valid trip date`,
    "date.empty": `trip date cannot be empty`,
    "any.required": `trip date is required`,
  }),
});

export default getSeatSchema;
