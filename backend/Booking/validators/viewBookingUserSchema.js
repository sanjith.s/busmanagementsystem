import Joi from "joi";

const viewBoookingUserSchema = Joi.object().keys({
  user_id: Joi.string().required().messages({
    "string.base": `Enter valid user id`,
    "string.empty": `User id cannot be empty`,
    "any.required": `User id is required`,
  }),
});

export default viewBoookingUserSchema;
