import Joi from "joi";

const cancelTicketSchema = Joi.object().keys({
  booking_id: Joi.string().required().messages({
    "string.base": `Enter valid booking id`,
    "string.empty": `Booking id cannot be empty`,
    "any.required": `Booking id is required`,
  }),
});

export default cancelTicketSchema;
