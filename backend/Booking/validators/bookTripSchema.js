import Joi from "joi";

const bookTripSchema = Joi.object().keys({
  user_id: Joi.string().required().messages({
    "string.base": `Enter valid user id`,
    "string.empty": `User id cannot be empty`,
    "any.required": `User id is required`,
  }),
  trip_id: Joi.string().required().messages({
    "string.base": `Enter valid trip id`,
    "string.empty": `Trip id cannot be empty`,
    "any.required": `Trip id is required`,
  }),
  passengers: Joi.array().required().messages({
    "array.base": `Enter passenger in array`,
    "array.empty": `Passengers cannot be empty`,
    "any.required": `Passengers is required`,
  }),
  bookedSeats: Joi.array()
    .length(Joi.ref("passengers.length"))
    .required()
    .messages({
      "array.base": `Enter booked seats in a array of Object`,
      "array.empty": `Booked seats cannot be empty`,
      "any.required": `Booked seats is required`,
      "array.length": `Booked seats and passengers size must be equal`,
    }),
  dateTime: Joi.date().required().messages({
    "date.base": `Enter valid date`,
    "date.empty": `Date cannot be empty`,
    "any.required": `Date is required`,
  }),
  totalPrice: Joi.number().min(10).required().messages({
    "string.base": `Enter valid total price`,
    "number.empty": `Total price cannot be empty`,
    "any.required": `Total price is required`,
  }),
});

export default bookTripSchema;
