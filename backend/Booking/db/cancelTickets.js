import Booking from "../models/booking_model.js";

// cancel tickets
const cancelTickets = async (booking_id) => {
  return await Booking.updateOne(
    { _id: booking_id },
    { $set: { isCancelled: true } },
    { runValidators: true }
  );
};

export default cancelTickets;
