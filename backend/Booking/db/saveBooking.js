import Booking from "../models/booking_model.js";
import { ObjectId } from "mongodb";

const saveBooking = async (
  user_id,
  trip_id,
  dateTime,
  bookedSeats,
  passengers,
  totalPrice
) => {
  const booking = new Booking({
    count: passengers.length,
    user_id: user_id,
    trip_id: new ObjectId(trip_id),
    trip_date: dateTime,
    bookedSeats: bookedSeats,
    passengers: passengers,
    totalPrice: totalPrice,
  });
  return await booking.save();
};

export default saveBooking;
