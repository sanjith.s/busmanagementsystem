import Booking from "../models/booking_model.js";

// user booking as previous , current and upcoming bookings

const viewUserBookings = async (user_id) => {
  // const prevBookings = await Booking.find({user_id:user_id , trip_date:{$lt : new Date().toISOString()} }).sort({trip_date:1})
  // const currBookings = await Booking.find({user_id:user_id , trip_date:{$gt : new Date().toISOString()} }).sort({trip_date:1})

  const prevBookings = await Booking.aggregate([
    {
      $match: {
        user_id: user_id, // Replace with the actual trip_id
        trip_date: { $lt: new Date() },
        isCancelled: false,
      },
    },
    {
      $lookup: {
        from: "trips", // Name of the trips collection
        localField: "trip_id",
        foreignField: "_id",
        as: "tripDetails",
      },
    },
  ]);
  const currBookings = await Booking.aggregate([
    {
      $match: {
        user_id: user_id,
        trip_date: { $gte: new Date() },
        isCancelled: false,
      },
    },
    {
      $lookup: {
        from: "trips",
        localField: "trip_id",
        foreignField: "_id",
        as: "tripDetails",
      },
    },
  ]);

  const cancelledBookings = await Booking.aggregate([
    {
      $match: {
        user_id: user_id,
        isCancelled: true,
      },
    },
    {
      $lookup: {
        from: "trips",
        localField: "trip_id",
        foreignField: "_id",
        as: "tripDetails",
      },
    },
  ]);
  return {prevBookings ,currBookings, cancelledBookings};
};

export default viewUserBookings;
