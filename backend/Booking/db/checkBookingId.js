import CustomError from "../../utils/CustomError.js";
import Booking from "../models/booking_model.js";

// check the validity of booking id
const checkBookingId = async(booking_id) =>{
    const check = await Booking.find({ _id: booking_id,isCancelled:false });
    if(check.length === 0){
        throw new CustomError("Invalid ticket id or Ticket already cancelled", 400);
    }
};

export default checkBookingId;