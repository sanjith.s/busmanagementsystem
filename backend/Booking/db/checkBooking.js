import CustomError from "../../utils/CustomError.js";
import Booking from "../models/booking_model.js";

// check whether the seat is available in the bus / not

const checkBooking = async (trip_id, dateTime, bookedSeats) => {
  const bookingCheck = await Booking.find({
    trip_id: trip_id,
    trip_date: new Date(dateTime),
    bookedSeats: { $in: bookedSeats },
  });

  const booking = bookingCheck.filter((booking) => {
    return booking.isCancelled !== true;
  });


  if (booking.length !== 0) {
    throw new CustomError("Seat already booked.Pick another seat", 400);
  }
  return bookingCheck;
};

export default checkBooking;
