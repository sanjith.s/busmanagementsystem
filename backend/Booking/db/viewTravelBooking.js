import CustomError from "../../utils/CustomError.js";
import Booking from "../models/booking_model.js";

const viewTravelBooking = async (trip_id, trip_date) => {
  const bookings = await Booking.find({
    trip_id: trip_id,
    isCancelled: false,
  });

  const bookList = bookings.filter((booking) => {
    return (
      new Date(booking.trip_date).toDateString() ===
      new Date(trip_date).toDateString()
    );
  });

  if (bookings.length === 0 || bookList.length === 0) {
    throw new CustomError("No bookings found :(", 400);
  }
  let booked_seats = 0;
  for (let i = 0; i < bookList.length; i++) {
    booked_seats += bookList[i].count;
  }
  return { booked_seats, bookList };
};

export default viewTravelBooking;
