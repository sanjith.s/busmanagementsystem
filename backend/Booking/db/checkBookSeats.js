import Booking from "../models/booking_model.js";

// check the count/availability of the seat matches with the booking
const checkBookSeats = async(trip_id, dateTime, bookedSeats,capacity) =>{
    const bookingCheck = await Booking.find({
        trip_id: trip_id,
        trip_date: new Date(dateTime),
        isCancelled: false,
      });
      const totalSeats = capacity;
      let bookSeats = 0;
      for (let j = 0; j < bookingCheck.length; j++) {
        bookSeats += bookingCheck[j].count;
      }

      const count = bookedSeats.length;
      return (count > totalSeats - bookSeats);
};

export default checkBookSeats;