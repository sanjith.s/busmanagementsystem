import CustomError from "../../utils/CustomError.js";
import Booking from "../models/booking_model.js";

const getSeat = async(trip_id,date)=>{
    const booking = await Booking.find({
        trip_id:trip_id,
        trip_date:new Date(date),
        isCancelled:false
    })
    let arr = [];
    for(let i=0;i<booking.length;i++){
        arr = [...arr , ...booking[i].bookedSeats]
    }
    return arr;
}

export default getSeat;

