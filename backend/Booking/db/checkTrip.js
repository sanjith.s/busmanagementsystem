import Trip from "../../Trips/models/trips_model.js";
import CustomError from "../../utils/CustomError.js";

// checking the validity of trip
const checkTrip = async (trip_id, dateTime) => {
  let tripCheck = await Trip.find({ _id: trip_id });
  if (tripCheck.length === 0) {
    throw new CustomError("Bus unavailable",500)
  }

  tripCheck = await Trip.find({
    _id: trip_id,
    available_days: { $in: [new Date(dateTime).getDay()] },
  });
  
  if (tripCheck.length === 0) {
    throw new CustomError("Trip unavailable",400);
  }
  return tripCheck[0].capacity;
};

export default checkTrip;
