// For accessing the variables in .env files..
import { config } from "dotenv";
config();

import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";
import bookTripService from "../services/bookTripService.js";
import viewBookingUserService from "../services/viewBookingUserService.js";
import viewBookingTravelService from "../services/viewBookingTravelService.js";
import cancelTicketService from "../services/cancelTicketService.js";
import {
  addBusMock,
  addTripMock,
  registerTravelMock,
  registerUserMock,
} from "../../utils/mock.js";

let _id; //for user id
let travel_id;
let bus_id;
let trip_id;
let booking_id;

beforeAll(async () => {
  const mongoServer = await MongoMemoryServer.create();
  const mongoUri = mongoServer.getUri();

  await mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  const user = await registerUserMock();
  _id = user._id.toString();
  const travel = await registerTravelMock();
  travel_id = travel._id.toString();
  bus_id = await addBusMock(travel_id);
  const trip = await addTripMock(bus_id);
  trip_id = trip._id.toString();
});

afterAll(async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
});

describe("Bookings", () => {
  const today = new Date();
  const tomorrow = today.setDate(today.getDate() + 1); //setting tomorrow date
  describe("Book trip service", () => {
    it("Book trips correctly", async () => {
      // Add booking
      const role = "user";
      const passengers = [
          {
            name: "abc",
            uniqueId: "45678",
          },
        ],
        bookedSeats = [1],
        dateTime = tomorrow,
        totalPrice = 100;
      const bookTrip = await bookTripService(
        _id,
        role,
        _id,
        trip_id,
        passengers,
        bookedSeats,
        dateTime,
        totalPrice
      );
      booking_id = bookTrip[0]._id.toString();

      // checking details
      expect(bookTrip[0]).toHaveProperty("seatNo");
      expect(bookTrip[0]).toHaveProperty("details");
      expect(bookTrip[0].details.name).toBe(passengers[0].name);
    });
    it("Booking already booked seats", async () => {
      // booking same seats again throws error
      const role = "user";
      const passengers = [
          {
            name: "abc",
            uniqueId: "45678",
          },
        ],
        bookedSeats = [1],
        dateTime = tomorrow,
        totalPrice = 100;
      const bookTrip = await bookTripService(
        _id,
        role,
        _id,
        trip_id,
        passengers,
        bookedSeats,
        dateTime,
        totalPrice
      ).catch((err) => {
        expect(err.statusCode).toBe(400);
        expect(err.status).toMatch(/fail/);
        expect(err.message).toMatch(/Seat already booked.Pick another seat/);
      });
    });
    it("Giving wrong details ", async () => {
      // passengers and booked seats count is unqual
      const role = "user";
      const passengers = [
          {
            name: "abc",
            uniqueId: "45678",
          },
        ],
        bookedSeats = [2, 3],
        dateTime = tomorrow,
        totalPrice = 100;
      const bookTrip = await bookTripService(
        _id,
        role,
        _id,
        trip_id,
        passengers,
        bookedSeats,
        dateTime,
        totalPrice
      ).catch((err) => {
        expect(err.statusCode).toBe(400);
        expect(err.status).toMatch(/fail/);
        expect(err.message).toMatch(
          /Booked seats and passengers size must be equal/
        );
      });
    });

    it("View user bookings", async () => {
      // view booking users..
      const role = "user";
      const userBooking = await viewBookingUserService(_id, role, _id);
      expect(userBooking).toHaveProperty("prevBookings");
      expect(userBooking).toHaveProperty("currBookings");
      expect(userBooking).toHaveProperty("cancelledBookings");
    });

    it("View travel bookings", async () => {
      // view travel bookings
      const role = "travels";
      const travelBooking = await viewBookingTravelService(
        role,
        trip_id,
        tomorrow
      );
      expect(travelBooking).toHaveProperty("booked_seats");
      expect(travelBooking).toHaveProperty("bookList");
      expect(travelBooking.bookList.length).toBe(travelBooking.booked_seats);
    });

    it("View travel bookings", async () => {
      // view travel bookings with wrong role
      const role = "user";
      const travelBooking = await viewBookingTravelService(
        role,
        trip_id,
        tomorrow
      ).catch((err) => {
        expect(err.message).toBe("Unauthorized");
        expect(err.statusCode).toBe(401);
      });
    });

    it("Cancel ticket", async () => {
      // Cancel tickets
      const role = "user";
      const cancelTicket = await cancelTicketService(role, booking_id);
      expect(cancelTicket).toHaveProperty("modifiedCount");
      expect(cancelTicket.acknowledged).toBe(true);
      expect(cancelTicket.modifiedCount).toBe(1);
    });

    it("Cancel ticket again", async () => {
      // Cancel tickets again for the same booking id
      const role = "user";
      const cancelTicket = await cancelTicketService(role, booking_id).catch(
        (err) => {
          expect(err.statusCode).toBe(400);
          expect(err.message).toBe(
            "Invalid ticket id or Ticket already cancelled"
          );
        }
      );
    });
  });
});
