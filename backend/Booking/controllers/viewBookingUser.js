// global try catch
import asyncErrorHandler from "../../utils/asyncErrorHandler.js";

// service functions
import viewBookingUserService from "../services/viewBookingUserService.js";

// user's booking
const viewBoookingUser = asyncErrorHandler(async (req, res) => {
  const { user_id } = req.params;

  const bookings = await viewBookingUserService(
    req.user.id,
    req.user.role,
    user_id
  );
  res.status(200).json({
    previousBookings: bookings.prevBookings,
    currentBookings: bookings.currBookings,
    cancelledBookings: bookings.cancelledBookings,
  });
});

export default viewBoookingUser;
