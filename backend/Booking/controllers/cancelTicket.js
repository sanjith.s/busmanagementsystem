// global try catch
import asyncErrorHandler from "../../utils/asyncErrorHandler.js";

// service function
import cancelTicketService from "../services/cancelTicketService.js";

const cancelTicket = asyncErrorHandler(async (req, res) => {
  const { booking_id } = req.body;
  await cancelTicketService(req.user.role, booking_id);
  res.status(200).send({ message: "Ticket cancelled successfully" });
});

export default cancelTicket;
