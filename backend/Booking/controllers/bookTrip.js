// global try catch
import asyncErrorHandler from "../../utils/asyncErrorHandler.js";
import bookTripService from "../services/bookTripService.js";

// Book trip
const bookTrip = asyncErrorHandler(async (req, res) => {
  const { user_id, trip_id, passengers, bookedSeats, dateTime, totalPrice } =
    req.body;
  const passengerList = await bookTripService(
    req.user.id,
    req.user.role,
    user_id,
    trip_id,
    passengers,
    bookedSeats,
    dateTime,
    totalPrice
  );

  res.status(200).send({
    message:
      "Ticket booked successfully for " + passengers.length + " passengers",
    passengers: passengerList,
  });
});

export default bookTrip;
