// TODO => If possible , do the available seats

// global try catch
import asyncErrorHandler from "../../utils/asyncErrorHandler.js";

//service functions
import viewBookingTravelService from "../services/viewBookingTravelService.js";

// Travels view booking
const viewBookingTravel = asyncErrorHandler(async (req, res) => {
  const { trip_id } = req.params;
  const { trip_date } = req.query;

  const bookings = await viewBookingTravelService(
    req.user.role,
    trip_id,
    trip_date
  );
  if (!bookings) {
    return res.status(200).send({ message: "No bookings found :-(" });
  }

  res.status(200).send({
    booked_seats: bookings.booked_seats,
    bookings: bookings.bookList,
  });
});

export default viewBookingTravel;
