import asyncErrorHandler from "../../utils/asyncErrorHandler.js";
import getSeatService from "../services/getSeatService.js";


const getSeats = asyncErrorHandler(async(req,res)=>{
    const {trip_id,date} = req.query;
    const tripSeats = await getSeatService(
        req.user.role,
        trip_id,
        date
    )
    res.status(200).send({seats:tripSeats})
})

export default getSeats;