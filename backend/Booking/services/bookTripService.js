import CustomError from "../../utils/CustomError.js";
import checkDate from "../../utils/checkDate.js";
import checkUserRole from "../../utils/checkUserRole.js";
import checkUserByID from "../../utils/checkUserByID.js";
import validate from "../../utils/validate.js";
import checkBookSeats from "../db/checkBookSeats.js";
import checkBooking from "../db/checkBooking.js";
import checkTrip from "../db/checkTrip.js";
import saveBooking from "../db/saveBooking.js";
import bookTripSchema from "../validators/bookTripSchema.js";

// book trip for user

const bookTripService = async (
  id,
  role,
  user_id,
  trip_id,
  passengers,
  bookedSeats,
  dateTime,
  totalPrice
) => {
  if (!checkUserByID(id, user_id)) {
    throw new CustomError("Enter valid user id (your id)", 400);
  }
  // Joi validation and role check
  if (
    checkUserRole(role) &&
    validate(bookTripSchema, {
      user_id,
      trip_id,
      passengers,
      bookedSeats,
      dateTime,
      totalPrice,
    })
  ) {
    if (!checkDate(dateTime)) {
      throw new CustomError("Enter correct date", 400);
    }
    const tripCheck = await checkTrip(trip_id, dateTime);
    if (!tripCheck) {
      throw new CustomError("Bus unavailable", 400);
    }

    // Check whether the seat is available in the bus
    const bookingCheck = await checkBooking(trip_id, dateTime, bookedSeats);

    if (bookingCheck) {
      const bookSeatsCheck = await checkBookSeats(
        trip_id,
        dateTime,
        bookedSeats,
        tripCheck
      );
      if (bookSeatsCheck) {
        throw new CustomError(" Tickets unavailable", 400);
      }

      // after passing these conditions ,trip can be booked
      const booking = await saveBooking(
        user_id,
        trip_id,
        dateTime,
        bookedSeats,
        passengers,
        totalPrice
      );
      const mapped = passengers.map((passenger, idx) => {
        return {
          _id: booking._id,
          seatNo: bookedSeats[idx],
          details: passenger,
        };
      });
      return mapped;
    }
  }
};

export default bookTripService;
