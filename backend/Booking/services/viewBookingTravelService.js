import checkTravelRole from "../../utils/checkTravelRole.js";
import validate from "../../utils/validate.js";
import viewTravelBooking from "../db/viewTravelBooking.js";
import viewBookingTravelSchema from "../validators/viewBookingTravelSchema.js";

// TODO => if possible , check whether the id is associated with the travels

const viewBookingTravelService = async(role, trip_id, trip_date) => {
  // role check and Joi validation
  if (
    checkTravelRole(role) &&
    validate(viewBookingTravelSchema, { trip_id, trip_date })
  ) {
    const bookings = await viewTravelBooking(trip_id, trip_date);
    return bookings;
  }
};

export default viewBookingTravelService;
