import CustomError from "../../utils/CustomError.js";
import checkUserRole from "../../utils/checkUserRole.js";
import validate from "../../utils/validate.js";
import cancelTickets from "../db/cancelTickets.js";
import checkBookingId from "../db/checkBookingId.js";
import cancelTicketSchema from "../validators/cancelTicketSchema.js";

const cancelTicketService = async (role, booking_id) => {
  if (checkUserRole(role) && validate(cancelTicketSchema, { booking_id })) {
    const check = await checkBookingId(booking_id);
    const cancelTicket = await cancelTickets(booking_id);
    if (!cancelTicket) {
      throw new CustomError("Something went wrong!", 500);
    }
    return cancelTicket;
  }
};

export default cancelTicketService;
