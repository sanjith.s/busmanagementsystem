import checkUserRole from "../../utils/checkUserRole.js";
import validate from "../../utils/validate.js";
import checkTrip from "../db/checkTrip.js";
import getSeat from "../db/getSeat.js";
import getSeatSchema from "../validators/getSeatSchema.js";

const getSeatService = async (role,trip_id,date)=>{
    if(checkUserRole(role) && validate(getSeatSchema , {trip_id,date})){
        await checkTrip(trip_id,date)
        const tripSeats = await getSeat(trip_id,date);
        return tripSeats;
    }
}

export default getSeatService;