import CustomError from "../../utils/CustomError.js";
import checkUserByID from "../../utils/checkUserByID.js";
import checkUserRole from "../../utils/checkUserRole.js";
import validate from "../../utils/validate.js";
import viewBoookingUserSchema from "../validators/viewBookingUserSchema.js";
import viewUserBookings from "../db/viewUserBooking.js"

const viewBookingUserService = async (id, role, user_id) => {
  if (!checkUserByID(id, user_id)) {
    throw new CustomError("Enter valid user id(your id)", 400);
  }
  if (checkUserRole(role) && validate(viewBoookingUserSchema, { user_id })) {
    const bookings = await viewUserBookings(user_id);
    return bookings;
  }
};

export default viewBookingUserService;
