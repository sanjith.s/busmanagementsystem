import mongoose from "mongoose";

const checkDate = (date)=>{
    return new Date(date).setHours(0,0,0,0) >=new Date().setHours(0,0,0,0);
};

const bookingSchema = new mongoose.Schema({
    // Booking id may be object _id
    count:{
        type:Number,
        required:[true,"Enter count"]
    },
    user_id:{
        type:String,
        required:[true,"Enter user id"],
        ref:'User'
    },
    trip_id:{
        type:mongoose.Types.ObjectId,
        required:[true,"Enter trip id"],
        ref:'Trip'
    },
    trip_date:{
        type:Date,
        required:[true,"Enter date"],
        validate:[checkDate,"Enter a valid date"]
    },
    date:{
        type:Date,
        default:Date.now(),
        required:[true,"Enter current time"]
    },
    bookedSeats:{
        type:Array,
        required:[true,"Enter seat numbers"]
    },
    passengers :{ //details of passenger
        type:Array,
        required:[true,"Enter passenger details"]
    },
    totalPrice:{
        type:Number,
        required:[true,"Enter total price"],
        min:[20,"Price can be atmost 20"]
    },
    isCancelled:{
        type:Boolean,
        default:false
    }
});

const Booking = mongoose.model("Booking", bookingSchema);

export default Booking;