import express from "express";
const router = express.Router();
router.use(express.json());
router.use(express.urlencoded({ extended: true }));

import authRouter from '../../Auth/routes/routes.js';
import bookingRouter from '../../Booking/routes/routes.js';
import busRouter from '../../Bus/routes/routes.js';
import tripRouter from '../../Trips/routes/routes.js';

// const authRouter = require('../../Auth/routes/routes.js');
// const bookingRouter = require('../../Booking/routes/routes.js')
// const busRouter = require('../../Bus/routes/routes.js')
// const tripRouter = require('../../Trips/routes/routes.js')

router.use(authRouter);
router.use(bookingRouter)
router.use(busRouter)
router.use(tripRouter);

export default router;
