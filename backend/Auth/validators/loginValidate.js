import Joi from "joi";
// const Joi = require("joi");

const loginSchema = Joi.object().keys({
  email: Joi.string()
    .email({
      minDomainSegments: 2,
      tlds: { allow: ["com", "net", "co", "org", "in"] },
    })
    .required()
    .messages({
      "email.base": `Enter email`,
      "email.empty": `Email cannot be empty`,
      "any.required": `Email is required`,
    })
    .required(),
  password: Joi.string().required().messages({
    "string.base": `Enter password`,
    "string.empty": `Password cannot be empty`,
    "any.required": `Password is required`,
  }),
});

export default loginSchema;
