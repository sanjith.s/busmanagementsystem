import Joi from "joi";

const userSchema = Joi.object().keys({
  name: Joi.string().required().messages({
    "string.base": `Enter name`,
    "string.empty": `Name cannot be empty`,
    "any.required": `Name is required`,
  }),
  email: Joi.string()
    .email({
      minDomainSegments: 2,
      tlds: { allow: ["com", "net", "co", "org", "in"] },
    })
    .messages({
      "string.base": `Enter email`,
      "string.empty": `Email cannot be empty`,
      "any.required": `Email is required`,
    })
    .required(),
  password: Joi.string()
    .min(6)
    .regex(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/
)
    .required()
    .messages({
      "string.base": `Enter password`,
      "string.min": "Password must be atleast of 6 characters",
      "string.regex.base": `Password should be minimum of 6 characters and contain letters or numbers only`,
      "object.regex": `Password should be minimum of 6 characters and contain letters or numbers only`,
      "string.empty": `Password cannot be empty`,
      "any.required": `Password is required`,
    }),
  role: Joi.string().valid("user", "travels").required().messages({
    "string.base": `Enter role `,
  }),
  phone: Joi.string()
    .regex(/^[0-9]{10}$/)
    .messages({
      "string.base": `Enter phone number`,
      "string.pattern.base": `Phone number must have 10 digits.`,
      "string.empty": `Phone number cannot be empty`,
      "any.required": `Phone number is required`,
    })
    .required(),
});

export default userSchema;
