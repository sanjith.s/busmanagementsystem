import User from "../models/user_model.js";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import CustomError from "../../utils/CustomError.js";

// checks whether the user is present in DB or not

const checkUser = async (email, password) => {
  let user = await User.find({ email: email });
  if (user.length === 1) {
    const passwordsMatch = await bcrypt.compare(password, user[0].password);
    if (passwordsMatch) {
      const role = user[0].role;
      const token = jwt.sign(
        { id: user[0]._id, email: user[0].email, role: role,name:user[0].name },
        process.env.TOKEN_SECRET.toString(),
        { expiresIn: "43200s" }
      );
      return {
        id: user[0]._id,
        email: email,
        name:user[0].name,
        token: token,
        role: user[0].role,
      };
    } else {
      throw new CustomError("Enter correct credentials", 400);
    }
  } else {
    throw new CustomError("Enter correct credentials", 400);
  }
};

export default checkUser;
