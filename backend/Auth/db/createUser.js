import User from "../models/user_model.js";
import bcrypt from "bcrypt";
const saltRounds = 10;

// utils function
import capitalizeWord from "../../utils/capitalizeWord.js";
import CustomError from "../../utils/CustomError.js";

// create user
const createUser = async (name, email, password, role, phone) => {
  // hash password using bcrypt
  const hashedPassword = await bcrypt.hash(password, saltRounds);
  const user = new User({
    name: capitalizeWord(name),
    email: email,
    password: hashedPassword,
    role: role,
    phone: phone,
  });
  const res = await user.save();
  if (!res) {
    throw new CustomError("Something went wrong", 500);
  }
  return res;
};

export default createUser;
