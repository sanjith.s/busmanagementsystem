// For accessing the variables in .env files..
import { config } from "dotenv";
config();

import mongoose from "mongoose";
import loginService from "../services/loginService";
import registerService from "../services/registerService";
import { MongoMemoryServer } from "mongodb-memory-server";
import { registerUserMock } from "../../utils/mock";

beforeAll(async () => {
  const mongoServer = await MongoMemoryServer.create();
  const mongoUri = mongoServer.getUri();

  await mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  // register a user
  await registerUserMock();
});

afterAll(async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
});

describe("User", () => {
  describe("Register", () => {
    it("Register a new user", async () => {
      const name = "user2";
      const email = "user2@gmail.com";
      const password = "user123";
      const role = "user";
      const phone = "9887766554";
      const user = await registerService(name, email, password, role, phone);
      expect(user.email).toBe(email);
      expect(user.role).toBe(role);
    });

    it("Register a existing user", async () => {
      const name = "user1";
      const email = "user1@gmail.com";
      const password = "user1234";
      const role = "user";
      const phone = "9887766554";
      await registerService(name, email, password, role, phone).catch((err) => {
        expect(err.message).toMatch(/E11000 duplicate key error collection/);
        expect(err.code).toBe(11000);
      });
    });
  });

  describe("Login", () => {
    it("Login user with correct credentials", async () => {
      const email = "user1@gmail.com";
      const password = "user123";
      const user = await loginService(email, password);
      expect(user).toHaveProperty("token");
      expect(user).toHaveProperty("email");
      expect(user.email).toBe(email);
    });
    it("Login user with wrong credentials", async () => {
      const email = "user1@gmail.com";
      const password = "user1234";
      await loginService(email, password).catch((err) => {
        expect(err.message).toBe("Enter correct credentials");
        expect(err.statusCode).toBe(400);
      });
    });
  });
});
