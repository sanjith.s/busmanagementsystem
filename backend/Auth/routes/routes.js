import express from "express";
const authRouter = express.Router();
authRouter.use(express.json());
authRouter.use(express.urlencoded({ extended: true }));

import loginRegisterItem from "../controllers/loginRegister.js";
import getTokenDetail from "../controllers/getTokenDetail.js";

import verifyToken from "../../middleware/verifyToken.js";

authRouter.get("/", (req, res) => {
  try {
    console.log("Welcome to Bus Management System - Login");
    res.send("Welcome");
  } catch (error) {
    res.send(error);
  }
});

authRouter.post("/register-user", loginRegisterItem.RegisterUser);

authRouter.post("/login-user", loginRegisterItem.LoginUser);

authRouter.get("/get-details", verifyToken, getTokenDetail);

export default authRouter;
