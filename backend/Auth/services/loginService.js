// validators
import loginSchema from "../validators/loginValidate.js";

// utils
import validate from "../../utils/validate.js";

// db calls
import checkUser from "../db/checkUser.js";

const loginService = async (email, password) => {
  // validation using Joi
  if (validate(loginSchema, { email, password })) {
    // checking details in DB
    const user = await checkUser(email, password);
    if (user) {
      return { id:user.id, email: user.email, name:user.name, token: user.token, role:user.role };
    }
  }
};

export default loginService;
