// validator functions
import userSchema from "../validators/registerValidate.js";

// utils
import validate from "../../utils/validate.js";

// db calls
import createUser from "../db/createUser.js";

const registerService = async (name, email, password, role, phone) => {
  if (validate(userSchema, { name, email, password, role, phone })) {
    // create user (Store in DB)
    return await createUser(name, email, password, role, phone);
  }
};

export default registerService;
