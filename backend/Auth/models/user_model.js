import mongoose from "mongoose";

var validateEmail = (email) => {
  var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return regex.test(email);
};

var validatePhone = (phone) => {
  return phone.toString().length === 10;
};

const userSchema = new mongoose.Schema({
  // user id omitted since it has a inbuild _id
  name: {
    type: String,
    required: [true, "Enter name"],
  },
  email: {
    type: String,
    required: [true, "Enter email"],
    unique: [true, "Email already exists"],
    lowercase: true,
    validate: [validateEmail, "Please fill a valid email address"],
    match: [
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      "Please fill a valid email address",
    ],
  },
  password: {
    type: String,
    required: [true, "Enter password"],
  },
  role: {
    type: String,
    required: [true, "Enter role"],
    enum: ["user", "travels"],
  },
  phone: {
    type: Number,
    trim: true,
    required: [true, "Enter phone number"],
    validate: [validatePhone, "Please enter a valid phone number"],
  },
});

const User = mongoose.model("User", userSchema);

// module.exports = User;
export default User;