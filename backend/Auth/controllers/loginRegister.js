// global try catch
import asyncErrorHandler from "../../utils/asyncErrorHandler.js";

// service functions
import loginService from "../services/loginService.js";
import registerService from "../services/registerService.js";

// Register user

const RegisterUser = asyncErrorHandler(async (req, res, next) => {
  const { name, email, password, role, phone } = req.body;
  await registerService(name, email, password, role, phone);
  res.status(201).send({ message: "User registered successfully" });
});

// login user

const LoginUser = asyncErrorHandler(async (req, res, next) => {
  const { email, password } = req.body;
  const user = await loginService(email, password);
  res.status(200).json({ id:user.id, email: user.email, name:user.name, token: user.token,role:user.role });
});

export default { RegisterUser, LoginUser };
