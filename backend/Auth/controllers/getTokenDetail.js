// global try catch
import asyncErrorHandler from "../../utils/asyncErrorHandler.js";

// get user details
const getTokenDetail = asyncErrorHandler((req, res, next) => {
  res
    .status(200)
    .send({ id: req.user.id, name:req.user.name , email: req.user.email, role: req.user.role });
});

export default getTokenDetail;
