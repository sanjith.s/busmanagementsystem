import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "../../axios/axiosConfig";

const initialState = {
  loading: "",
  trips: [],
  error: "",
};

export const addViewTrip = createAsyncThunk(
  "trip/addViewTrip",
  async (data) => {
    // console.log(data);
    const res = await axios.get(
      `/view-trip?source=${data.source}&destination=${data.destination}&datetime=${data.date}`
    );
    // console.log("in tripSlice", res);
    return {trips : res.data.trip};
  }
);

const tripSlice = createSlice({
  name: "trip",
  initialState,
  extraReducers: (builder) => {
    builder.addCase(addViewTrip.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(addViewTrip.fulfilled, (state, action) => {
      state.loading = false;
      state.trips = action.payload.trips;
      state.error = "";
    });
    builder.addCase(addViewTrip.rejected, (state, action) => {
      state.loading = false;
      state.trips = [];
      state.error = action.error.message;
    });
  },
  // reducers: {
  //   addTrip: (state, action) => {
  //     console.log("Trip", action.payload.trip);
  //     state.trips = action.payload.trip;
  //   }
  // },
});

// export const { addTrip } = tripSlice.actions;

export default tripSlice.reducer;
