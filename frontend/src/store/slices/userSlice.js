import { createSlice } from "@reduxjs/toolkit";
import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "../../axios/axiosConfig";
// import axios from "axios";

const initialState = {
  id: "",
  email: "",
  name: "",
  role: "", //optional one
  loading: false,
  error: "",
};

// Export loginUser thunk..
export const loginUser = createAsyncThunk(
  "user/loginUser",
  async (userData) => {
    // try {
    const res = await axios.post("/login-user", userData);
    const data = res.data;
    const token = data.token;
    sessionStorage.setItem("accessToken", token);
    // console.log(data);
    return {
      id: data.id,
      email: data.email,
      name: data.name,
      role: data.role,
    };
    // } catch (error) {
    //   throw error;
    // }
  }
);

// Export get details if possible
export const getDetails = createAsyncThunk("user/getDetails", async () => {
  // try {
  const res = await axios.get("/get-details");
  const data = res.data;
  // console.log(data);
  return {
    id: data.id,
    email: data.email,
    name: data.name,
    role: data.role,
  };
  // } catch (error) {
  //   console.log(error);
  //   throw error;
  // }
});

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    removeUser: (state, action) => {
      state.id = "";
      state.email = "";
      state.name = "";
      state.role = "";
    },
  },

  extraReducers: (builder) => {
    // For login
    builder.addCase(loginUser.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(loginUser.fulfilled, (state, action) => {
      state.id = action.payload.id;
      state.email = action.payload.email;
      state.name = action.payload.name;
      state.role = action.payload.role;
      state.loading = false;
      state.error = "";
    });
    builder.addCase(loginUser.rejected, (state, action) => {
      state.id = "";
      state.email = "";
      state.name = "";
      state.role = "";
      state.loading = false;
      state.error = action.error.message;
    });

    // For get details
    builder.addCase(getDetails.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getDetails.fulfilled, (state, action) => {
      state.id = action.payload.id;
      state.email = action.payload.email;
      state.name = action.payload.name;
      state.role = action.payload.role;
      state.loading = false;
      state.error = "";
    });
    builder.addCase(getDetails.rejected, (state, action) => {
      state.id = "";
      state.email = "";
      state.name = "";
      state.role = "";
      state.loading = false;
      state.error = action.error.message;
    });
  },
});

export const { removeUser } = userSlice.actions;



export default userSlice.reducer;
