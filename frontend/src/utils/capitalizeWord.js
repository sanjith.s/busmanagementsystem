
const capitalizeWord = (string)=>{
    const word = string.trim();
    const first = word.charAt(0).toUpperCase();
    const last = word.slice(1).toLowerCase();
    return first+last;
}

export default capitalizeWord;