import { Routes, Route, Navigate, useNavigate } from "react-router-dom";
// import Layout from '../src/layout/layout'
import { Fragment, useEffect } from "react";
// common
import Login from "./pages/Common/Login";
import Register from "./pages/Common/Register";

// user
import Home from "./pages/users/Home";
import Booking from "./pages/users/Booking";
import "./App.css";
import ViewTrips from "./pages/users/ViewTrips";
import BookTrip from "./pages/users/BookTrip";
import ViewBooking from "./pages/users/ViewBooking";
import PrivateRoutes from "./components/PrivateRoutes";
import { ErrorBoundary } from "react-error-boundary";
import Error from "./components/Error";
import CustomNav from "./components/Home/CustomNav";
import Footer from "./components/Home/Footer";

function App() {

  return (
    <Routes>
      <Route path="/" element={<Navigate replace to="/login" />} />
      <Route path="/login" element={<Login />} exact />
      <Route path="/register" element={<Register />} exact />
      <Route element={<PrivateRoutes/>}>
        <Route path="/home" element={<Home />} />
        <Route path="/book-ticket" element={<Booking />} />
        <Route path="/view-trips" element={<ViewTrips />} />
        <Route path="/book-trips" element={<BookTrip />} />
        <Route path="/view-booking" element={<ViewBooking />} />
      </Route>
      <Route path="*" element={<Navigate replace to="/home" />} />
    </Routes>
  );
}

export default App;
