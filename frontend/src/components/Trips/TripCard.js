import React from "react";
import "./TripCard.css"; // Import the custom CSS file
import { FaClock } from "react-icons/fa";
import { useNavigate } from "react-router-dom";

const TripCard = (props) => {


  const navigate = useNavigate();
  //   if possible add total seats
  const {
    tripId,
    date,
    source,
    destination,
    ratings,
    travelName,
    availableSeats,
    bookedSeats,
    price,
    startTime,
    duration,
  } = props;

  return (
    <div className="trip-card">
      <div className="trip-image">
        <img
          src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSE68s1_Lp3Bwki0ECR3w6jxHKFA9n0sJK_89YCgFAOSRQ0OQe5RHI03M-jr2Vae-uybw0&usqp=CAU"
          alt="Trip"
        />
      </div>
      <div className="trip-details">
        <h2>Travel Name : </h2>
        <h3>{travelName}</h3>
        <h3>Ratings: {ratings}</h3>
      </div>
      <div className="trip-seats">
        <p>
          From: {source} - To: {destination}
        </p>

        <p>Available Seats: {availableSeats}</p>
        <p>Booked Seats: {bookedSeats}</p>
      </div>
      <div className="trip-seats">
        <p className="price-details">Price: ₹{price}</p>
        <p>
          <strong>
            <FaClock />
          </strong>{" "}
          {startTime}
        </p>
        <p>Duration : {duration}</p>
      </div>
      <div className="book-button">
        <button
          onClick={() => {
            return navigate(`/book-trips?trip_id=${tripId}&date=${date}`);
          }}
        >
          Book Tickets
        </button>
      </div>
    </div>
  );
};

export default TripCard;
