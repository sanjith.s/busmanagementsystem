import React, { useEffect, useState } from "react";

import { Navigate, Outlet } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getDetails } from "../store/slices/userSlice";
// import { addUser } from "../store/slices/userSlice";

import { Audio } from "react-loader-spinner";
import Footer from "./Home/Footer";
import CustomNav from "./Home/CustomNav";
import { ErrorBoundary } from "react-error-boundary";
import Error from "../components/Error";

const PrivateRoutes = () => {
  const dispatch = useDispatch();
  let authToken = sessionStorage.getItem("accessToken");
  // console.log(authToken);
  const loader = useSelector((state) => state.user.loading);
  if (loader === true) {
    <Audio height="100" width="100" color="grey" ariaLabel="loading" />;
  }
  let details;
  useEffect(() => {
    const callApi = async () => {
      const details = await dispatch(getDetails());
      return details;
    };
    details = callApi();
  }, []);

  // optional one since it was handled in Error boundary
  if (details?.error?.message === "Forbidden") {
    return <Navigate to="/login" />;
  }

  if (!loader) {
    return authToken ? <ErrorBoundary FallbackComponent={Error}><CustomNav/><Outlet /><Footer/></ErrorBoundary> : <Navigate to="/login" />;
  }
};
export default PrivateRoutes;
