import React, { useState } from "react";
import "./Card.css"; // Import the custom CSS file for styling
import { BsFillBusFrontFill, BsFillCalendarMinusFill } from "react-icons/bs";
import { Button } from "reactstrap";
import axios from "../../axios/axiosConfig";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate } from "react-router-dom";

const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Friday", "Saturday"];
const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

const Card = ({
  bookingId,
  date,
  source,
  destination,
  travelName,
  count,
  bookedDate,
  bookedSeats,
  passengers,
  totalPrice,
  time,
  duration,
  busName,
  buttonDisable,
}) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [loading, setLoading] = useState(false);

  // For error boundary
  const [error, setError] = useState(false);
  const navigate = useNavigate();

  const cancelTicket = async (ticket_id) => {
    const data = { booking_id: ticket_id };
    try {
      setLoading(true);
      const res = await axios.post("/cancel-ticket", data);
      if (res.status === 200) {
        toast.success("Ticket cancelled successfully", {
          position: toast.POSITION.TOP_RIGHT,
        });
        setTimeout(() => {
          navigate("/view-booking");
        }, 2000);
        window.location.reload();
      }
    } catch (error) {
      // console.log(error);
      setError(true);
      toast.error(error.message, {
        position: toast.POSITION.TOP_RIGHT,
      });
    } finally {
      setLoading(false);
    }

    // console.log("Ticket cancel", ticket_id);
  };

  const handleOpenModal = () => {
    setIsModalOpen(true);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  if (error) {
    throw new Error("Forbidden");
  }

  return (
    <>
      <div className="card" onClick={handleOpenModal}>
        <div className="card-title">
          <div className="card-content">
            <center>
              <BsFillBusFrontFill size={"40px"} />
            </center>
            <center>
              <h1 className="head1" style={{ marginTop: "15px" }}>
                {new Date(date).getDate()}
              </h1>
            </center>
            <h2 className="head2">{days[new Date(date).getDay()]}</h2>
          </div>
          <div className="card-content">
            <h3
              className="head3"
              style={{ marginTop: "-2px", marginBottom: "20px" }}
            >
              {count} Bus ticket
            </h3>
            <h2 className="head2">{source} -</h2>
            <h2 className="head2">{destination}</h2>
            <h2
              className="head2"
              style={{ textTransform: "uppercase", marginTop: "10px" }}
            >
              {travelName}
            </h2>
          </div>
        </div>
        <hr style={{ marginTop: "-2px" }} />
        <div className="card-title">
          <h3 className="head3" style={{ color: "rgb(74,73,73)" }}>
            {months[new Date(date).getMonth()]} {new Date(date).getFullYear()}
          </h3>
          <h2 className="head2">Boarding: {source}</h2>
        </div>
      </div>

      {/* Modal */}
      {isModalOpen && (
        <div className="modal">
          <div className="modal-content">
            <span className="close" onClick={handleCloseModal}>
              &times;
            </span>
            <center>
              <h1 className="head1">
                <BsFillCalendarMinusFill style={{ marginRight: "10px" }} />
                <i style={{ fontWeight: "400" }}>Booked on : </i>
                {new Date(bookedDate).toDateString()} ,
              </h1>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-around",
                  marginTop: "25px",
                }}
              >
                <h1 className="head3">
                  {" "}
                  <i style={{ fontWeight: "500" }}>Source :</i> {source}
                </h1>
                <h1 className="head3">
                  {" "}
                  <i style={{ fontWeight: "500" }}>Destination :</i>{" "}
                  {destination}
                </h1>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-around",
                  marginTop: "15px",
                }}
              >
                <h2 className="head3">
                  <i style={{ fontWeight: "500" }}>Date:</i> {date}
                </h2>
                <h2 className="head3">
                  <i style={{ fontWeight: "500" }}>Time:</i> {time}
                </h2>
                <h2 className="head3">
                  <i style={{ fontWeight: "500" }}>Duration:</i> {duration}
                </h2>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-around",
                  marginTop: "15px",
                }}
              >
                <h2 className="head3">
                  <i style={{ fontWeight: "500" }}>Bus name:</i> {busName}
                </h2>
                <h2 className="head3">
                  <i style={{ fontWeight: "500" }}>Total price : </i>{" "}
                  {totalPrice}
                </h2>
              </div>
            </center>
            {/* Additional passenger details */}
            <hr />
            <div className="passenger-details">
              {bookedSeats.map((seat, index) => {
                return (
                  <div key={index}>
                    <h4>Passenger {index + 1}</h4>
                    <p>Seat No : {seat}</p>
                    <p>Name: {passengers[index].name} </p>
                    <p>Unique id : {passengers[index].uniqueId} </p>
                  </div>
                );
              })}
            </div>
            {!buttonDisable && (
              <Button
                active
                color="warning"
                outline
                onClick={async () => {
                  await cancelTicket(bookingId);
                  handleCloseModal();
                }}
              >
                {loading ? "loading" : "Cancel Ticket"}
              </Button>
            )}
          </div>
          <ToastContainer />
        </div>
      )}
    </>
  );
};

export default Card;
