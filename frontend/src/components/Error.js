import React, { useEffect } from "react";
import { NavLink } from "react-router-dom";

import { useDispatch } from "react-redux";
import { removeUser } from "../store/slices/userSlice";

const Error = ({ error }) => {
  const dispatch = useDispatch();
  // console.log("ErRoR", error);
  useEffect(() => {
    dispatch(removeUser());
  }, []);
  return (
    <center>
      <img style={{ height: "60vh" }} src="img/errorImg.webp" alt="error" />
      <h2>Refresh the page or Try login again</h2>
      <h3 style={{ margin: "15px", fontSize: "20px" }}>
        Error : {error.message}
      </h3>
      <button>
        <NavLink style={{ textDecoration: "none" }} to="/login">
          Login
        </NavLink>
      </button>
    </center>
  );
};

export default Error;
