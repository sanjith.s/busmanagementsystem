import React from "react";
import "./Advantages.css"; // Import the custom CSS file

const Advantages = () => {
  const advantages = [
    {
      title: "Easy online booking",
      image: "img/easyroute.png", // Replace with your image URL
    },
    {
      title: "Crores of Happy Customers",
      image: "img/happy.png", // Replace with your image URL
    },
    {
      title: "Real-time tracking of buses",
      image: "img/bus.png", // Replace with your image URL
    },
    {
      title: "Instant Booking & Refund options",
      image: "img/ticket.png", // Replace with your image URL
    },
    {
      title: "24/7 customer support",
      image: "img/service.png", // Replace with your image URL
    },
  ];

  return (
    <div style={{marginBottom:'30px'}}>
      <br />
      <center><h2>Advantages</h2></center>
      <br/>
      <div className="advantages-container">
        {advantages.map((advantage, index) => (
          <div key={index} className="advantage-item">
            <img
              src={advantage.image}
              alt={advantage.title}
              className="advantage-image"
            />
            <p className="advantage-title">{advantage.title}</p>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Advantages;
