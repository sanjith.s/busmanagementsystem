import React from "react";
import "./Footer.css"; // Import the custom CSS file
import { FaFacebook, FaTwitter, FaInstagram, FaLinkedin } from "react-icons/fa";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="footer-text">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tempor
          magna eu nisi aliquam, at ultrices lectus viverra
        </p>
      </div>
      <div className="footer-map">
        <img
          src="https://developers.google.com/static/maps/images/landing/hero_geocoding_api.png"
          alt="Map"
          className="map-image"
        />
      </div>
      <div className="footer-address">
        <p>123 Main Street,</p>
        <p>Banglore</p>
        <p>India</p>
      </div>
      <hr className="footer-hr" />
      <div className="footer-left">
        <p>&copy; Ticket Booking</p>
      </div>
      <div className="footer-right">
        <a href="/home" className="social-icon">
          <FaFacebook />
        </a>
        <a href="/home" className="social-icon">
          <FaTwitter />
        </a>
        <a href="/home" className="social-icon">
          <FaInstagram />
        </a>
        <a href="/home" className="social-icon">
          <FaLinkedin />
        </a>
      </div>
    </footer>
  );
};

export default Footer;
