import React from "react";
import { NavLink } from "react-router-dom";
import "./CustomNav.css";
import { useDispatch } from "react-redux";
import { removeUser } from "../../store/slices/userSlice";

const CustomNav = () => {
  const dispatch = useDispatch();
  return (
    <nav className="horizontal-navbar">
      <div className="navbar-left">
        <NavLink to="/" className="navbar-logo">
          Ticket Booking
        </NavLink>
      </div>
      <div className="navbar-right">
        <ul>
          <li>
            <NavLink  to="/home" >
              Home
            </NavLink>
          </li>
          <li>
            <NavLink to="/book-ticket">
              Book tickets
            </NavLink>
          </li>
          <li>
            <NavLink to="/view-booking">
              View Booking
            </NavLink>
          </li>
          <li>
            <NavLink to="/login" onClick={async()=>{
              await dispatch(removeUser())
              window.location.reload();  //for clearing redux
              sessionStorage.clear();
            }}>
              Logout
            </NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default CustomNav;
