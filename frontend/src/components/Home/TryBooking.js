import React from "react";
import "./TryBooking.css"; // Import the custom CSS file
import { NavLink } from "react-router-dom";

const TryBooking = () => {
  return (
    <div className="try-login-container">
      <div className="try-now">
        <h2>Want to Try?</h2>
        <p style={{ fontSize: "20px" }}>
          Book tickets by entering souce & destination
        </p>
        <button className="try-button">
          <NavLink
            style={{ textDecoration: "none", color: "white" }}
            to="/book-ticket"
          >
            Book tickets
          </NavLink>
        </button>
      </div>
      <div className="login">
        <h2>See Bookings then :-)</h2>
        <br />
        <button className="login-button">
          <NavLink
            style={{ textDecoration: "none", color: "white" }}
            to="/view-booking"
          >
            View Bookings
          </NavLink>
        </button>
      </div>
    </div>
  );
};

export default TryBooking;
