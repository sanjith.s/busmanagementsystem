import React, { useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "./BusSearch.css"; // Import the custom CSS file
import capitalizeWord from "../../utils/capitalizeWord";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

// redux
import { useSelector } from "react-redux";

// pages
import ViewTrips from "../../pages/users/ViewTrips";
import { useNavigate } from "react-router-dom";

const BusSearch = () => {
  const [source, setSource] = useState();
  const [destination, setDestination] = useState();
  const [date, setDate] = useState();

  const user = useSelector((state) => state.user);
  const navigate = useNavigate();
  const sourceHandler = (event) => {
    const src = event.target.value.trim();
    if (src.length === 0) {
      toast.error("Enter Source", {
        position: toast.POSITION.TOP_RIGHT,
      });
      return;
    }
    setSource(capitalizeWord(src));
  };

  const destinationHandler = (event) => {
    const dst = event.target.value.trim();
    if (dst.length === 0) {
      toast.error("Enter Destination", {
        position: toast.POSITION.TOP_RIGHT,
      });
      return;
    }
    setDestination(capitalizeWord(dst));
  };

  const handleSearch = (event) => {
    // Add your search logic here
    event.preventDefault();
    if(source === '' || destination === ''){
      toast.error("Enter different source and Destination", {
        position: toast.POSITION.TOP_RIGHT,
      });
      return;
    }
    if (source === destination) {
      toast.error("Enter different source and Destination", {
        position: toast.POSITION.TOP_RIGHT,
      });
      return;
    }
    navigate(
      `/view-trips?source=${source}&destination=${destination}&date=${date.toDateString()}`
    );
  };

  return (
    <div className="bus-search-container">
      <div className="bus-search-form">
        <center>
          <h2 className="header-title"> Search Your Bus</h2>
        </center>
        <form onSubmit={handleSearch}>
          <div className="form-row">
            <label style={{fontSize:"20px"}} htmlFor="from">From:</label>
            <input
              placeholder="source"
              style={{ width: "30%" }}
              type="text"
              id="from"
              name="from"
              onBlur={sourceHandler}
              onChange={sourceHandler}
            />
          </div>
          <div className="form-row">
            <label style={{fontSize:"20px"}} htmlFor="to">To:</label>
            <input
              placeholder="destination"
              style={{ width: "30%" }}
              type="text"
              id="to"
              name="to"
              onBlur={destinationHandler}
              onChange={destinationHandler}
            />
          </div>
          <div className="form-row">
            <label style={{fontSize:"20px"}} htmlFor="date">Date:</label>
            <DatePicker
              placeholderText="Enter date"
              dateFormat="dd/MM/yyyy"
              dayClassName={() => "example-datepicker-day-class"}
              popperClassName="example-datepicker-class"
              todayButton="TODAY"
              minDate={new Date()}
              selected={date}
              onChange={(date) => setDate(date)}
            />
          </div>
          <button type="submit" className="search-button">
            Search
          </button>
        </form>
      </div>
      <div className="bus-search-image">
        <img
          src="https://static.vecteezy.com/system/resources/thumbnails/010/200/115/small/internet-service-for-book-and-buy-bus-ticket-travel-and-tourism-concept-tourist-planning-trip-online-passengers-buying-tickets-for-bus-in-mobile-app-vector.jpg"
          alt="Bus"
          className="bus-images"
        />
      </div>
      <ToastContainer />
    </div>
  );
};

export default BusSearch;
