import axios from "../../axios/axiosConfig";
import { BallTriangle } from "react-loader-spinner";
import React, { useEffect, useState } from "react";
import "./SeatPicker.css";
import { useSelector } from "react-redux";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import CustomNav from "../Home/CustomNav";
import Footer from "../Home/Footer";
import { useNavigate } from "react-router-dom";

import { useErrorBoundary } from "react-error-boundary";
import { Button } from "reactstrap";

const SeatPicker = ({ tripId, date, seatsPerRow = 5 }) => {
  // Generate seat numbers based on the provided number of rows and seats per row
  const { showBoundary } = useErrorBoundary();
  const [bookedSeats, setBookedSeats] = useState([]);
  const [loading, setLoading] = useState(true);
  const [availableSeats, setAvailableSeats] = useState(0);

  const [currentTrip, setCurrentTrip] = useState();
  // redux
  const [error, setError] = useState(false);
  const [errorMsg, setErrorMsg] = useState();
  const [bookError, setBookError] = useState(false);
  // above for error boundary
  const userId = useSelector((state) => state.user.id);
  const allTrips = useSelector((state) => state.trip.trips);
  // console.log("ALl", allTrips);

  // navigate
  const navigate = useNavigate();

  if (allTrips.length === 0) {
    navigate("/book-ticket");
  }

  useEffect(() => {
    const getBookedSeats = async () => {
      try {
        const res = await axios.get(
          `/get-seats?trip_id=${tripId}&date=${new Date(date).toDateString()}`
        );

        for (let i = 0; i < allTrips.length; i++) {
          if (allTrips[i]._id === tripId) {
            // console.log(allTrips[i]);
            setCurrentTrip(allTrips[i]);
            setAvailableSeats(allTrips[i].capacity);
          }
        }
        // console.log(res);
        const bookedSeatsData = res.data.seats;
        setBookedSeats(bookedSeatsData);
      } catch (error) {
        // console.log("Error : ", error);
        setErrorMsg(error.message);
        setError(true); //throws here catches to error boundary
      } finally {
        setLoading(false);
      }
    };
    getBookedSeats();
  }, []);

  if (error) {
    throw new Error(errorMsg);
  }

  // console.log("Trips available", allTrips);

  const [passengerNames, setpassengerNames] = useState([]);
  const [uniqueId, setUniqueId] = useState([]);

  const isSeatBooked = (seatNumber) => {
    for (let i = 0; i < bookedSeats.length; i++) {
      if (bookedSeats[i] === seatNumber) {
        return true;
      }
    }
    return false;
    // return bookedSeats.includes(seatNumber);
  };

  const generateSeatNumbers = () => {
    const seatNumbers = [];
    const rows = availableSeats / seatsPerRow;
    for (let row = 1; row <= rows; row++) {
      for (let seat = 1; seat <= seatsPerRow; seat++) {
        if (seat === 3) {
          seatNumbers.push(null); // Add a gap after the second seat
        }
        seatNumbers.push(`${row}${String.fromCharCode(64 + seat)}`);
      }
    }

    return seatNumbers;
  };

  const seatNumbers = generateSeatNumbers();

  // for selected seats
  const [selectedSeats, setSelectedSeats] = useState([]);

  // Option 1 for throwing error
  // const errr = new Error("Error occured");
  // throw errr;
  // Option 2 for throwing error
  // showBoundary(Error("Errrrrorr occur"));
  // Function - seat selection

  const handleSeatSelection = (seatNumber) => {
    if (seatNumber === null) return; // Ignore the gap
    if (isSeatBooked(seatNumber)) return; //Ignore the booking seats
    if (selectedSeats.includes(seatNumber)) {
      // If the seat is already selected, remove it from the selectedSeats , passengerNames and uniqueId array
      setSelectedSeats(selectedSeats.filter((seat) => seat !== seatNumber));
      passengerNames.pop();
      uniqueId.pop();
    } else {
      // If the seat is not selected, add it to the selectedSeats array
      setSelectedSeats([...selectedSeats, seatNumber]);
    }
  };

  // Function to handle changes in passenger details input boxes
  const handlepassengerNamesChange = (index, value) => {
    const updatedpassengerNames = [...passengerNames];

    updatedpassengerNames[index] = value.trim();

    setpassengerNames(updatedpassengerNames);
  };

  // Function to handle changes in passenger details input boxes
  const handlePassengerIdChange = (index, value) => {
    const updatedPassengerIdDetails = [...uniqueId];

    updatedPassengerIdDetails[index] = value.trim();

    setUniqueId(updatedPassengerIdDetails);
  };

  const submitDetail = async (event) => {
    event.preventDefault();
    if (
      passengerNames.length === uniqueId.length &&
      passengerNames.length === selectedSeats.length
    ) {
      const passengers = [];
      passengerNames.forEach((element, index) => {
        if (element.length != 0 && uniqueId[index].length !== 0) {
          passengers.push({ name: element, uniqueId: uniqueId[index] });
        } else {
          toast.error("Enter passenger detail", {
            position: toast.POSITION.BOTTOM_RIGHT,
          });
          return;
        }
      });

      const total = passengerNames.length * currentTrip.price;
      const bookingData = {
        user_id: userId,
        trip_id: currentTrip._id,
        passengers: passengers,
        bookedSeats: selectedSeats,
        dateTime: date,
        totalPrice: total,
      };
      // console.log(bookingData);
      try {
        const res = await axios.post("/book-trip", bookingData);
        if (res?.status === 200) {
          toast.success("Booking successful", {
            position: toast.POSITION.TOP_RIGHT,
          });
          setTimeout(() => {
            navigate("/view-booking");
          }, 2000);
        }
        return;
      } catch (error) {
        // console.log(error);
        toast.error(`Something went wrong! ${error.message}`, {
          position: toast.POSITION.TOP_RIGHT,
        });
        setBookError(true);
        // return;
      }
    }
    toast.error("Enter valid detail", {
      position: toast.POSITION.BOTTOM_RIGHT,
    });
  };

  if (bookError) {
    throw new Error("Something went wrong !");
  }

  if (loading) {
    return (
      <div
        style={{ marginTop: "35vh", marginLeft: "45%", marginBottom: "35vh" }}
      >
        <BallTriangle
          height="100"
          width="100"
          color="grey"
          ariaLabel="loading"
        />
      </div>
    );
  }

  if (!loading) {
    return (
      <div>
        {/* <CustomNav /> */}
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-around",
          }}
        >
          <div className="seat-picker">
            <h2 className="header">Select your seats here :)</h2>
            <div className="seat-layout">
              {seatNumbers.map((seatNumber, index) => (
                <div
                  key={seatNumber}
                  className={`seat ${seatNumber === null ? "gap" : ""} ${
                    selectedSeats.includes(seatNumber) ? "selected" : ""
                  } ${isSeatBooked(seatNumber) ? "booked" : ""}`}
                  onClick={() => handleSeatSelection(seatNumber)}
                >
                  {seatNumber !== null ? seatNumber : ""}
                </div>
              ))}
            </div>
          </div>

          {/* Dynamic input */}
          {availableSeats !== bookedSeats.length ? (
            <div style={{ marginLeft: "15%", marginRight: "10%" }}>
              {selectedSeats.length != 0 && (
                <h2 className="header-title">Passenger details here</h2>
              )}
              {selectedSeats.map((seatNumber, index) => (
                <div key={seatNumber}>
                  <input
                    style={{ width: "150px" }}
                    // key={seatNumber}
                    type="text"
                    placeholder={`Passenger ${index + 1}`}
                    onChange={(e) =>
                      handlepassengerNamesChange(index, e.target.value)
                    }
                  />
                  <input
                    style={{ width: "150px" }}
                    // key={index}
                    type="text"
                    placeholder={`Unique id ${index + 1}`}
                    onChange={(e) =>
                      handlePassengerIdChange(index, e.target.value)
                    }
                  />
                </div>
              ))}
              {selectedSeats.length !== 0 && (
                <center>
                  <button type="submit" onClick={submitDetail}>
                    Submit
                  </button>
                  <h2 className="head1">
                    Total price : {selectedSeats.length * currentTrip.price}
                  </h2>
                </center>
              )}
            </div>
          ) : (
            <div style={{ display: "flex", flexDirection: "column" }}>
              <img
                style={{ height: "70vh", width: "70vh", marginTop: "6%" }}
                src="img/noSeats.jpg"
                alt="No bus found"
              />
              <Button
                onClick={() => {
                  navigate(-1);
                }}
                style={{ fontSize: "20px" }}
              >
                View Alternate trips
              </Button>
            </div>
          )}
        </div>
        {/* <Footer /> */}
        <ToastContainer />
      </div>
    );
  }
};

export default SeatPicker;
