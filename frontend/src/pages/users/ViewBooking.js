import React, { useDebugValue, useEffect, useState } from "react";
import CustomNav from "../../components/Home/CustomNav";
import Footer from "../../components/Home/Footer";
import axios from "../../axios/axiosConfig";
import { useDispatch, useSelector } from "react-redux";

import { Audio } from "react-loader-spinner";
import Card from "../../components/Bookings/Card";
import { getDetails, removeUser } from "../../store/slices/userSlice";
import { useNavigate } from "react-router-dom";

const ViewBooking = () => {
  const [bookings, setBookings] = useState([]);
  const [loading, setLoading] = useState(true);
  // For error boundary
  const [error, setError] = useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  // console.log("DEtailS user", user);

  useEffect(() => {
    const bookingsAPI = async () => {
      try {
        const res = await axios.get(`/view-bookings-user/${user?.id}`);
        // console.log("ReS", res.data);
        if (res !== undefined) {
          setBookings(res.data);
        }
      } catch (error) {
        // console.log(error);
        setError(true);
      } finally {
        setLoading(false);
      }
    };
    bookingsAPI();
  }, []);

  if (error) {
    // thrown here and catch by error boundary
    throw new Error("Forbidden");
  }

  if (loading) {
    return (
      <div style={{ marginTop: "40vh", marginLeft: "45%" , marginBottom:"40vh" }}>
        <Audio height="100" width="100" color="grey" ariaLabel="loading" />
      </div>
    );
  }

  return (
    <div>
      {/* <CustomNav /> */}
      {bookings.previousBookings.length === 0 &&
        bookings.currentBookings.length === 0 &&
        bookings.cancelledBookings.length === 0 && (
          <center>
            <h2 style={{ margin: "10%" }}>No bookings</h2>
          </center>
        )}
      {/* current bookings */}
      {bookings.previousBookings.length !== 0 && (
        <>
          <center>
            <h2 className="header-title">Previous Bookings</h2>
          </center>
          <div
            style={{
              display: "flex",
              flexWrap: "wrap",
              justifyContent: "space-around",
            }}
          >
            {bookings.previousBookings.map((prevBooking, index) => {
              return (
                <Card
                  key={prevBooking._id}
                  bookingId={prevBooking._id}
                  date={new Date(prevBooking.trip_date).toDateString()}
                  source={prevBooking.tripDetails[0].source}
                  destination={prevBooking.tripDetails[0].destination}
                  travelName={"Travels"}
                  count={prevBooking.count}
                  bookedDate={prevBooking.date}
                  bookedSeats={prevBooking.bookedSeats}
                  passengers={prevBooking.passengers}
                  totalPrice={prevBooking.totalPrice}
                  time={prevBooking.tripDetails[0].dateTime}
                  duration={prevBooking.tripDetails[0].duration}
                  busName={prevBooking.tripDetails[0].bus_id}
                  buttonDisable={true}
                />
              );
            })}
          </div>
        </>
      )}

      {/* current bookings */}
      {bookings.currentBookings.length !== 0 && (
        <>
          <center>
            <h2 className="header-title">Current Bookings</h2>
          </center>
          <div
            style={{
              display: "flex",
              flexWrap: "wrap",
              justifyContent: "space-around",
            }}
          >
            {bookings.currentBookings.map((currBooking, index) => {
              return (
                <Card
                  key={currBooking._id}
                  bookingId={currBooking._id}
                  date={new Date(currBooking.trip_date).toDateString()}
                  source={currBooking.tripDetails[0].source}
                  destination={currBooking.tripDetails[0].destination}
                  travelName={"Travels"}
                  count={currBooking.count}
                  bookedDate={currBooking.date}
                  bookedSeats={currBooking.bookedSeats}
                  passengers={currBooking.passengers}
                  totalPrice={currBooking.totalPrice}
                  time={currBooking.tripDetails[0].dateTime}
                  duration={currBooking.tripDetails[0].duration}
                  busName={currBooking.tripDetails[0].bus_id}
                  buttonDisable={false}
                />
              );
            })}
          </div>
        </>
      )}

      {/* cancelled bookings */}
      {bookings.cancelledBookings.length !== 0 && (
        <>
          <center>
            <h2 className="header-title">Cancelled Bookings</h2>
          </center>
          <div
            style={{
              display: "flex",
              flexWrap: "wrap",
              justifyContent: "space-around",
            }}
          >
            {bookings.cancelledBookings.map((cancelledBooking, index) => {
              return (
                <Card
                  key={cancelledBooking._id}
                  bookingId={cancelledBooking._id}
                  date={new Date(cancelledBooking.trip_date).toDateString()}
                  source={cancelledBooking.tripDetails[0].source}
                  destination={cancelledBooking.tripDetails[0].destination}
                  travelName={"Travels"}
                  count={cancelledBooking.count}
                  bookedDate={cancelledBooking.date}
                  bookedSeats={cancelledBooking.bookedSeats}
                  passengers={cancelledBooking.passengers}
                  totalPrice={cancelledBooking.totalPrice}
                  time={cancelledBooking.tripDetails[0].dateTime}
                  duration={cancelledBooking.tripDetails[0].duration}
                  busName={cancelledBooking.tripDetails[0].bus_id}
                  buttonDisable={true}
                />
              );
            })}
          </div>
        </>
      )}

      {/* <Footer /> */}
    </div>
  );
};

export default ViewBooking;
