import React from "react";
import { Navigate, useNavigate } from "react-router-dom";
import CustomNav from "../../components/Home/CustomNav";
import Footer from "../../components/Home/Footer";
import BusSearch from "../../components/Bus/BusSearch";

const Booking = () => {
  if (!window.sessionStorage.getItem("accessToken"))
    return <Navigate to="/login" />;

  return (
    <div>
      <BusSearch />
    </div>
  );
};

export default Booking;
