import React from "react";
import { Navigate } from "react-router-dom";
import CustomCarousel from "../../components/Home/CustomCarousel";
import Footer from "../../components/Home/Footer";
import Advantages from "../../components/Home/Advantages";
import TryBooking from "../../components/Home/TryBooking";
import CustomNav from "../../components/Home/CustomNav";
// get details from redux toolkit
import { useSelector } from "react-redux";

const Home = () => {
  const user = useSelector((state) => state.user);
  if (!sessionStorage.getItem("accessToken"))
    return <Navigate to="/login" />;
  return (
    <div>
      {/* <CustomNav /> */}
      <CustomCarousel />
      <Advantages />
      <TryBooking />
      {/* <Footer /> */}
    </div>
  );
};

export default Home;
