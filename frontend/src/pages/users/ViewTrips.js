import { useEffect, useState } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
// import axios from "../../axios/axiosConfig";
import TripCard from "../../components/Trips/TripCard";

import { Audio } from "react-loader-spinner";

import { useDispatch, useSelector } from "react-redux";
import { addViewTrip } from "../../store/slices/tripSlice";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

// import { getDetails } from "../../store/slices/userSlice";

const ViewTrips = (props) => {
  const loading = useSelector((state) => state.trip.loading);
  const error = useSelector((state) => state.trip.error);
  // const trips = useSelector((state) => state.trip.trips);
  const [trips, setTrip] = useState([]);
  const [searchParams, setSearchParams] = useSearchParams();
  const source = searchParams.get("source");
  const destination = searchParams.get("destination");
  const date = searchParams.get("date");

  const user = useSelector((state) => state.user);

  const dispatch = useDispatch();
  const navigate = useNavigate();
  // console.log("User details", user);

  useEffect(() => {
    const callApi = async () => {
      // console.log("In view trips");
      const trip = await dispatch(addViewTrip({ source, destination, date }));
      if (trip.payload !== undefined) {
        setTrip(trip.payload.trips);
      }
      // No need of catch since we have 'error' parameter in slice
    };

    callApi();
  }, []);

  if (loading) {
    return (
      <div
        style={{ marginTop: "40vh", marginLeft: "45%", marginBottom: "40vh" }}
      >
        <Audio height="100" width="100" color="grey" ariaLabel="loading" />
      </div>
    );
  }

  if (error === "Forbidden") {
    throw new Error("Forbidden");
  }

  if (error !== "Forbidden" && error) {
    // console.log(error);
    toast.error(`Error : ${error}`, {
      position: toast.POSITION.TOP_RIGHT,
    });
    navigate(-1);
  }

  return (
    <div>
      {/* <CustomNav /> */}

      {error !== "Forbidden" && error && !loading && (
        <center>
          <img src="img/noBusFound.png" alt="image" />
        </center>
      )}

      {!error &&
        !loading &&
        trips.length !== 0 &&
        trips.map((trip, index) => (
          <TripCard
            key={trip._id}
            tripId={trip._id}
            date={date}
            source={trip.source}
            destination={trip.destination}
            ratings={5}
            travelName={"Travels "}
            availableSeats={trip.capacity - trip.filled_seats}
            bookedSeats={trip.filled_seats}
            price={trip.price}
            startTime={trip.dateTime}
            duration={trip.duration}
          />
        ))}

      {/* {!loading && <Footer/>} */}
      <ToastContainer />
    </div>
  );
};

export default ViewTrips;
