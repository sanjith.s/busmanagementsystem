import { Audio } from "react-loader-spinner";
import { useState } from "react";

import SeatPicker from "../../components/Bus/seatPicker";

import { useSearchParams } from "react-router-dom";
import { ErrorBoundary } from "react-error-boundary";

import Error from "../../components/Error";

const BookTrip = () => {
  // const [loading, setLoading] = useState(false);
  const [searchParams, setSearchParams] = useSearchParams();
  const trip_id = searchParams.get("trip_id");
  const date = searchParams.get("date");
  return (
    <ErrorBoundary FallbackComponent={Error}>
      <SeatPicker tripId={trip_id} date={date} />
    </ErrorBoundary>
  );
};

export default BookTrip;
