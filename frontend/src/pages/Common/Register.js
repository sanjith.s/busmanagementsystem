import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Form, FormGroup, Label, Input, Button, Card, Row } from "reactstrap";
import { Link, useNavigate } from "react-router-dom";

import axios from "../../axios/axiosConfig";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Register = () => {
  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [userPassword, setUserPassword] = useState("");
  const [role, setRole] = useState("user");
  const [phone, setPhone] = useState(0);
  const [showPassword, setShowPassword] = useState(false);

  const mailHandler = (event) => {
    if (event.target.value.trim().length === 0) {
      toast.error("Enter email first", {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
    setEmail(event.target.value);
  };

  const passwordHandler = (event) => {
    const pass = event.target.value.trim();
    let regex =
      /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;

    if (pass.trim().length === 0 && regex.test(pass)) {
      // console.log(pass);
      toast.error("Enter strong password", {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
    setUserPassword(pass);
  };

  const nameHandler = (event) => {
    if (event.target.value.trim().length === 0) {
      toast.error("Enter name first", {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
    setName(event.target.value);
  };

  const roleHandler = (event) => {
    if (event.target.value.trim().length === 0) {
      toast.error("Enter role", {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
    setRole(event.target.value);
  };

  const phoneHandler = (event) => {
    if (event.target.value.trim().length === 0) {
      toast.error("Enter valid phone number", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } else if (event.target.value.trim().length !== 10) {
      toast.error("Enter correct phone number", {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
    setPhone(event.target.value);
  };

  const checkBoxHandler = () => {
    setShowPassword(!showPassword);
  };

  const registerHandler = async (event) => {
    event.preventDefault();
    const userData = {
      name: name,
      email: email,
      password: userPassword,
      role: role,
      phone: phone,
    };
    // console.log(userData);
    try {
      const res = await axios.post("/register-user", userData);
      // console.log("Res", res);
      if (res?.status === 201) {
        toast.success("Registration successful", {
          position: toast.POSITION.TOP_RIGHT,
        });
        navigate("/login");
      }
    } catch (error) {
      // console.log("Error", error);
      if (error?.status === 400) {
        toast.error(error?.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
        setTimeout(() => {
          navigate('/login')
        }, 1000);
        
      } else if (error?.status === 403) {
        toast.error(error?.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
        ("unauthorized");
      } else {
        toast.error("Something went wrong !", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    }
  };

  return (
    <Card
      style={{
        width: "90%",
        maxWidth: "30rem",
        margin: "2% auto",
        padding: "2rem",
        // marginTop:'15%'
      }}
    >
      <Form onSubmit={registerHandler}>
        <FormGroup>
          <Label>
            <h3>Register</h3>
          </Label>
        </FormGroup>
        <FormGroup>
          <Label for="exampleNamw">Name</Label>
          <Input
            id="exampleName"
            name="name"
            placeholder="enter name"
            type="text"
            onBlur={nameHandler}
            onChange={nameHandler}
          />
        </FormGroup>
        <FormGroup>
          <Label for="exampleEmail">Email</Label>
          <Input
            id="exampleEmail"
            name="email"
            placeholder="with a placeholder"
            type="email"
            onBlur={mailHandler}
            onChange={mailHandler}
          />
        </FormGroup>
        <FormGroup>
          <Label for="examplePassword">Password</Label>
          <Input
            id="examplePassword"
            name="password"
            placeholder="password placeholder"
            type={showPassword ? "text" : "password"}
            onBlur={passwordHandler}
            onChange={passwordHandler}
          />
        </FormGroup>
        <FormGroup check>
          <Input type="checkbox" onChange={checkBoxHandler} />{" "}
          <Label check>See password</Label>
        </FormGroup>
        <FormGroup>
          <Label for="exampleSelect">Role:</Label>
          {/* <Col sm={10}> */}
          <Input
            id="exampleSelect"
            name="select"
            type="select"
            onChange={roleHandler}
          >
            <option value={"user"}>User</option>
            <option value={"travels"}>Travels</option>
          </Input>
          {/* </Col> */}
        </FormGroup>
        <FormGroup>
          <Label for="examplePhone">Phone</Label>
          <Input
            id="examplePhone"
            name="phone"
            placeholder="enter phone"
            type="number"
            onBlur={phoneHandler}
            onFocus={phoneHandler}
          />
        </FormGroup>

        <Button style={{ marginTop: "20px" }}>Register</Button>
        <br />
        <Row style={{ marginTop: "15px" }}>
          <Link style={{ textDecoration: "none" }} to="/login">
            Login here
          </Link>
        </Row>
      </Form>
      <ToastContainer />
    </Card>
  );
};

export default Register;
