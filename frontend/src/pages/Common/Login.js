import React, { useState } from "react";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import { loginUser } from "../../store/slices/userSlice";

import { Form, FormGroup, Label, Input, Button, Card, Row } from "reactstrap";
import { ToastContainer, toast } from "react-toastify";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-toastify/dist/ReactToastify.css";

import { Audio } from "react-loader-spinner";

const Login = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [email, setEmail] = useState("");
  const [userPassword, setUserPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const loading = useSelector((state) => state.user.loading);
  const error = useSelector((state) => state.user.error);

  const mailHandler = (event) => {
    if (event.target.value.trim().length === 0) {
      toast.error("Enter email first", {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
    setEmail(event.target.value);
  };

  const passwordHandler = (event) => {
    if (event.target.value.trim().length === 0) {
      toast.error("Enter password", {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
    setUserPassword(event.target.value);
  };

  const checkBoxHandler = () => {
    setShowPassword(showPassword => !showPassword);
  };

  const loginHandler = async (event) => {
    event.preventDefault();
    const userData = {
      email: email,
      password: userPassword,
    };

    await dispatch(loginUser(userData));
    navigate("/home");

    // catch not necessary since error was managed by error..
  };

  if (error) {
    toast.error(`Something went wrong !${error}`, {
      position: toast.POSITION.TOP_RIGHT,
    });
  }

  if (loading && sessionStorage.getItem("accessToken")) {
    sessionStorage.clear();
    return <Navigate to="/login" />;
  }

  if (loading) {
    return (
      <div style={{ marginTop: "35vh", marginLeft: "45%" }}>
        <Audio height="100" width="100" color="grey" ariaLabel="loading" />
      </div>
    );
  }

  return (
    <Card
      style={{
        width: "90%",
        maxWidth: "30rem",
        margin: "6% auto",
        padding: "2rem",
      }}
    >
      <Form onSubmit={loginHandler}>
        <FormGroup>
          <Label>
            <h3>Login</h3>
          </Label>
        </FormGroup>
        <FormGroup>
          <Label for="exampleEmail">Email</Label>
          <Input
            id="exampleEmail"
            name="email"
            placeholder="with a placeholder"
            type="email"
            onBlur={mailHandler}
            onChange={mailHandler}
          />
        </FormGroup>
        <FormGroup>
          <Label for="examplePassword">Password</Label>
          <Input
            id="examplePassword"
            name="password"
            placeholder="password placeholder"
            type={showPassword ? "text" : "password"}
            onBlur={passwordHandler}
            onChange={passwordHandler}
          />
        </FormGroup>

        <FormGroup check>
          <Input type="checkbox" onChange={checkBoxHandler} />{" "}
          <Label check>See password</Label>
        </FormGroup>

        <Button style={{ marginTop: "20px" }}>Login</Button>
        <br />
        <Row style={{ marginTop: "15px" }}>
          <Link style={{ textDecoration: "none" }} to="/register">
            Register
          </Link>
        </Row>
      </Form>
      <ToastContainer />
    </Card>
  );
};

export default Login;
